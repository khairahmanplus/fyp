<?php 
    include('partials-front/menu.php'); 
    // Check whether food id is set or not
    if(isset($_GET['food_id']))
    {
        // Get the Food id and details of the selected food
        $food_id = $_GET['food_id'];

        // Get the details of the selected food
        $sql = "SELECT * FROM food WHERE id=$food_id";
        // Execute the Query
        $res = mysqli_query($conn, $sql);
        // Count the rows
        $count = mysqli_num_rows($res);
        // Check whether the data is available or not
        if($count==1)
        {
            // We have data
            // Get the data from Database
            $row = mysqli_fetch_assoc($res);

            $title = $row['title'];
            $desc = $row['description'];
            $price = $row['price'];
            $image_name = $row['image_name'];
            $category_id = $row['category_id'];
        }
        else
        {
            // Food not available
            // Redirect to Home Page
            header('location:'.SITEURL);
            exit;
        }
        if ($category_id==1){
            $is_cake=1;
        }
        else{
            $is_cake=0;
        }
    }
    else
    {
        // Redirect to homepage
        header('location:'.SITEURL);
        exit;
    }

    // Handle review submission, update, and delete
    if(isset($_SESSION['username']))
    {
        $username = $_SESSION['username'];
        // Get customer id from username
        $cust_sql = "SELECT id FROM customers WHERE username='$username'";
        $cust_res = mysqli_query($conn, $cust_sql);
        if(mysqli_num_rows($cust_res) == 1)
        {
            $cust_row = mysqli_fetch_assoc($cust_res);
            $custId = $cust_row['id'];

            // Check if the customer already reviewed this food item
            $review_check_sql = "SELECT * FROM review WHERE custId='$custId' AND foodId='$food_id'";
            $review_check_res = mysqli_query($conn, $review_check_sql);
            $review_exists = mysqli_num_rows($review_check_res) > 0;
            
            // Handle review update
            if(isset($_POST['updateReview']) && $review_exists)
            {
                $rating = mysqli_real_escape_string($conn, $_POST['rating']);
                $reviewMessage = mysqli_real_escape_string($conn, $_POST['reviewMessage']);
                $timeSubmitted = date('Y-m-d H:i:s');

                $update_review_sql = "UPDATE review SET rating='$rating', reviewMessage='$reviewMessage', timeSubmitted='$timeSubmitted' 
                                      WHERE custId='$custId' AND foodId='$food_id'";
                mysqli_query($conn, $update_review_sql);

                // Redirect to the same page to show the updated review
                header("Location: {$_SERVER['PHP_SELF']}?food_id=$food_id");
                exit;
            }

            // Handle review delete
            if(isset($_POST['deleteReview']) && $review_exists)
            {
                $delete_review_sql = "DELETE FROM review WHERE custId='$custId' AND foodId='$food_id'";
                mysqli_query($conn, $delete_review_sql);

                // Redirect to the same page after deleting the review
                header("Location: {$_SERVER['PHP_SELF']}?food_id=$food_id");
                exit;
            }

            // Handle new review submission
            if(isset($_POST['submitReview']) && !$review_exists)
            {
                $rating = mysqli_real_escape_string($conn, $_POST['rating']);
                $reviewMessage = mysqli_real_escape_string($conn, $_POST['reviewMessage']);
                $timeSubmitted = date('Y-m-d H:i:s');

                $insert_review_sql = "INSERT INTO review (custId, foodId, rating, reviewMessage, timeSubmitted) 
                                      VALUES ('$custId', '$food_id', '$rating', '$reviewMessage', '$timeSubmitted')";
                mysqli_query($conn, $insert_review_sql);

                // Redirect to the same page to show the new review
                header("Location: {$_SERVER['PHP_SELF']}?food_id=$food_id");
                exit;
            }
        }
    }
?>

<div class="container">
    <form name="cart" action="cart.php" method="POST" class="add">
        <fieldset>
            <div class="food-menu-img">
                <?php 
                    // Check whether the image is available or not
                    if($image_name == "")
                    {
                        // Image not available
                        echo "<div class='error'>Image not Available.</div>";
                    }
                    else
                    {
                        // Image is available
                        ?>
                        <img src="<?php echo SITEURL; ?>images/food/<?php echo $image_name; ?>" alt="<?php echo $title; ?>" class="img-responsive img-curve">
                        <?php
                    }
                ?>
            </div>

            <div class="food-menu-desc">
                <h2 class="text-center text-black"><?php echo $title; ?></h2>
                <p class="food-price"><?php echo $desc; ?></p>
                <input type="hidden" name="is_cake" value="<?php echo $is_cake; ?>">
                <input type="hidden" name="is_custom" value="0">
                <input type="hidden" name="food" value="<?php echo $title; ?>">
                <input type="hidden" name="id" value="<?php echo $food_id; ?>">
                <input type="hidden" name="form_submitted" value="1">
                <h3>RM<?php echo $price; ?></h3>
                <input type="hidden" name="price" value="<?php echo $price; ?>">
                <div class="order-label">Quantity</div>
                <input type="number" name="qty" class="input-responsive w3-border" value="1" required>

                <?php if ($category_id == 1) : ?>
                    <a href="<?php echo SITEURL; ?>customize-cake.php?cake_id=<?php echo $food_id; ?>" class="w3-button w3-round-large w3-green">Go to Customize Cake page</a>
    
<?php 
    if(isset($_SESSION['order']))
    {
        echo $_SESSION['order'];
        unset($_SESSION['order']);
    }
?><h4>Or (for our default deco):</h4>
    <input type="submit" name="addToCart" value="Add To Cart" class="w3-button w3-round-large w3-green">

        
<?php else: ?>

                <input type="submit" name="addToCart" value="Add To Cart" class="w3-button w3-round-large w3-green">
<?php endif; ?>
            </div>
        </fieldset>
    </form>

    <!-- Review Section Starts Here -->
    <div class="review-section">
        <h3>Customer Reviews</h3>
        
        <?php
            // Fetch and display reviews
            $review_sql = "SELECT r.rating, r.reviewMessage, r.timeSubmitted, c.username FROM review r 
                           JOIN customers c ON r.custId = c.id 
                           WHERE r.foodId = $food_id 
                           ORDER BY r.timeSubmitted DESC";
            $review_res = mysqli_query($conn, $review_sql);
            $review_count = mysqli_num_rows($review_res);

            if($review_count > 0)
            {
                while($review_row = mysqli_fetch_assoc($review_res))
                {
                    echo "<div class='review'>";
                    echo "<p><strong>{$review_row['username']}</strong> - {$review_row['timeSubmitted']}</p>";
                    echo "<p>Rating: {$review_row['rating']}/10</p>";
                    echo "<p>{$review_row['reviewMessage']}</p>";
                    echo "</div>";
                }
            }
            else
            {
                echo "<p>No reviews yet.</p>";
            }
        ?>

        <?php if(isset($_SESSION['username'])): ?>
            <?php if($review_exists): ?>
                <?php 
                    // Fetch existing review details
                    $existing_review = mysqli_fetch_assoc($review_check_res); 
                ?>
                <form action="" method="POST" class="review-form">
                    <h4>Edit Your Review</h4>
                    <label for="rating">Rating:</label>
                    <input type="number" name="rating" id="rating" min="1" max="10" value="<?php echo $existing_review['rating']; ?>" required>/10
                    <br><br>
                    <textarea name="reviewMessage" id="reviewMessage" rows="4" placeholder="Write your review here" style="width: 480px; height: 95px;" required><?php echo $existing_review['reviewMessage']; ?></textarea>
                    <br><br>
                    <input type="submit" name="updateReview" value="Update Review" class="btn btn-primary
">
<input type="submit" name="deleteReview" value="Delete Review" class="btn btn-danger">
</form>
<?php else: ?>
<form action="" method="POST" class="review-form">
<h4>Leave a Review</h4>
<label for="rating">Rating:</label>
<input type="number" name="rating" id="rating" min="1" max="10" required>/10
<br><br>
<textarea name="reviewMessage" id="reviewMessage" rows="4" placeholder="Write your review here" style="width: 480px; height: 95px;" required></textarea>
<br><br>
<input type="submit" name="submitReview" value="Submit Review" class="btn btn-primary">
</form>
<?php endif; ?>
<?php else: ?>
<p>Please <a href="login.php">login</a> to leave a review.</p>
<?php endif; ?>
</div>
<!-- Review Section Ends Here -->

</div>
<?php include('partials-front/footer.php'); ?>