<?php
include('partials-front/menu.php'); 

// Check if the user is logged in
if (!isset($_SESSION['username'])) {
    header('location:login.php');
    exit;
}

$cust_username = $_SESSION['username'];

// Fetch user details
$user_sql = "SELECT * FROM customers WHERE username='$cust_username'";
$user_res = mysqli_query($conn, $user_sql);
$user = mysqli_fetch_assoc($user_res);

// Update user details
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_info'])) {
    $full_name = mysqli_real_escape_string($conn, $_POST['full_name']);
    $contact = mysqli_real_escape_string($conn, $_POST['contact']);
    $address = mysqli_real_escape_string($conn, $_POST['address']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    
    $update_sql = "UPDATE customers SET full_name='$full_name', contact_no='$contact', address='$address', email='$email' WHERE username='$cust_username'";
    $update_res = mysqli_query($conn, $update_sql);
    
    if ($update_res) {
        echo "<div class='success'>Profile updated successfully.</div>";
    } else {
        echo "<div class='error'>Failed to update profile. Please try again.</div>";
    }
}

// Fetch past orders
$order_sql = "SELECT * FROM orders WHERE custId='{$user['id']}' ORDER BY order_date DESC";
$order_res = mysqli_query($conn, $order_sql);

// Fetch customization library
$custom_sql = "SELECT c.*, f.title as foodName FROM customization c
               JOIN food f ON c.foodId = f.id
               WHERE c.custId='{$user['id']}'";
$custom_res = mysqli_query($conn, $custom_sql);

?>

<div class="container">
    <h1>Profile</h1>
    
    <!-- Edit Personal Info Section -->
    <div class="section">
        <h2>Edit Personal Info</h2>
        <form action="" method="POST">
            <div class="order-label">Full Name</div>
            <input type="text" name="full_name" value="<?php echo $user['full_name']; ?>" class="input-responsive w3-border" required>

            <div class="order-label">Phone Number</div>
            <input type="tel" name="contact" value="<?php echo $user['contact_no']; ?>" class="input-responsive w3-border" required>

            <div class="order-label">Email</div>
            <input type="email" name="email" value="<?php echo $user['email']; ?>" class="input-responsive w3-border" required>

            <div class="order-label">Address</div>
            <textarea name="address" rows="10" class="input-responsive w3-border" required><?php echo $user['address']; ?></textarea>

            <input type="submit" name="update_info" value="Update Info" class="btn btn-primary"/>
        </form>
    </div>
    <!-- View Past Orders Section -->
    <div class="section">
        <h2>Past Orders</h2>
        <?php if (mysqli_num_rows($order_res) > 0): ?>
            <table class="w3-table-all">
                <tr>
                    <th>Order ID</th>
                    <th>Order Date</th>
                    <th>Total Amount</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                <?php while ($order = mysqli_fetch_assoc($order_res)): ?>
                <tr>
                    <td><?php echo $order['id']; ?></td>
                    <td><?php echo $order['order_date']; ?></td>
                    <td><?php echo $order['total']; ?></td>
                    <td><?php echo $order['status']; ?></td>
                    <td>
                        <button id="pdfButton_<?php echo $order['id']; ?>" class="w3-button w3-blue" onclick="printReceipt(<?php echo $order['id']; ?>)">Print Receipt</button>
                        <div id="generatePDF_<?php echo $order['id']; ?>" style="display: none;">
                            <?php
                                // Inline PHP to fetch order details and generate receipt content
                                $order_sql_details = "SELECT * FROM orders WHERE id='{$order['id']}' AND custId='{$user['id']}'";
                                $order_res_details = mysqli_query($conn, $order_sql_details);
                                if (mysqli_num_rows($order_res_details) > 0) {
                                    $order_details = mysqli_fetch_assoc($order_res_details);
                                    $customer_name = $order_details['customer_name']; 
                                    $payment = $order_details['payment'];
                                    $order_date = $order_details['order_date'];
                                    $foods = $order_details['food'];

                                    echo "<fieldset style='max-width:600px;margin:auto;'>";
                                    echo "<h4 style='text-align:center'>PAYMENT RECEIPT⠀⠀⠀⠀⠀⠀⠀⠀Auntie Jue Home Bakes</h4>";
                                    echo "<table style='width:100%;border:1px solid black'>";
                                    echo "<tr><td>Bill to: $customer_name</td><td style='text-align:right'>Receipt No.: {$order_details['id']}</td></tr>";
                                    echo "<tr><td>Payment method: $payment</td><td style='text-align:right'>Order date and time: $order_date</td></tr>";
                                    echo "</table><br>";
                                    
                                    echo "<div style='text-align:right'>Item (Price x Quantity = Total)<br>";
                                    echo "$foods<br>";
                                    echo "</div>";
                                    
                                    echo "<div style='text-align:center'><br>Thank you for your business!</div>";
                                    echo "</fieldset>";
                                }
                            ?>
                        </div>
                    </td>
                </tr>
                <?php endwhile; ?>
            </table>
        <?php else: ?>
            <p>You have no past orders.</p>
        <?php endif; ?>
    </div>

    <!-- View Customization Library Section -->
    <div class="section" id="design-library">
        <h2>Design Library</h2>
        <?php
    if(isset($_SESSION['order']))
    {
        echo $_SESSION['order'];
        unset($_SESSION['order']);
    } ?>
        <?php if (mysqli_num_rows($custom_res) > 0): ?>
            <table class="w3-table-all">
                <tr>
                    <th>Design ID</th>
                    <th>Food Name</th>
                    <th>Base Colour</th>
                    <th>Decoration Colour</th>
                    <th>Toppings</th>
                    <th>Cream Writing</th>
                    <th>Action</th>
                </tr>
                <?php while ($custom = mysqli_fetch_assoc($custom_res)): ?>
                <tr>
                    <td><?php echo $custom['id']; ?></td>
                    <td><?php echo $custom['foodName']; ?></td>
                    <td><?php echo $custom['baseColour']; ?></td>
                    <td><?php echo $custom['decoColour']; ?></td>
                    <td><?php echo $custom['toppings']; ?></td>
                    <td><?php echo $custom['creamWrite']; ?></td>
                    <td>
                        <a href="cart.php?custom_id=<?php echo $custom['id']; ?>" class="w3-button w3-green w3-round">Add to Cart</a>
                        <a href="del-design.php?custom_id=<?php echo $custom['id']; ?>" class="w3-button w3-red w3-round">Delete</a>
                    </td>
                </tr>
                <?php endwhile; ?>
            </table>
        <?php else: ?>
            <p>You have no saved designs.</p>
        <?php endif; ?>
    </div>
</div>
<script>
        function printReceipt(orderId) {
            var button = document.getElementById("pdfButton_" + orderId);
            var makepdf = document.getElementById("generatePDF_" + orderId);
            
            var mywindow = window.open("", "PRINT", "height=600,width=600");
            mywindow.document.write(makepdf.innerHTML);
            mywindow.document.close();
            mywindow.focus();
            mywindow.print();
            return true;
        }
    </script>

<?php include('partials-front/footer.php'); ?>
