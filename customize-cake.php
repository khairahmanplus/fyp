<?php
include('partials-front/menu.php');
if (isset($_GET['cake_id']) && isset($_SESSION['username'])) {
    $cake_id = $_GET['cake_id'];
    $sql = "SELECT * FROM food WHERE id=$cake_id AND category_id=1"; // Ensure it's a cake
    $res = mysqli_query($conn, $sql);
    $count = mysqli_num_rows($res);
    
    if ($count == 1) {
        $row = mysqli_fetch_assoc($res);
        $title = $row['title'];
        $price = $row['price'];
        $description = $row['description'];
        $image_name = $row['image_name'];
    } else {
        header('location:' . SITEURL);
        exit;
    }
} else {
    $_SESSION['order'] = "<div class='error'>You need to login to customize cake. <a class='w3-text-green' href='login.php'>Login Here</a></div>";
    header('location:' . SITEURL.'food-item.php?food_id='.$_GET['cake_id'].'');
    exit;
}
?>

<div class="container">
    <form action="cart.php" method="POST" class="customize-form" onsubmit="concatenateToppings()">
        <fieldset>
            <div class="food-menu-img">
                <?php 
                if ($image_name == "") {
                    echo "<div class='error'>Image not Available.</div>";
                } else {
                    ?>
                    <img src="<?php echo SITEURL; ?>images/food/<?php echo $image_name; ?>" alt="<?php echo $title; ?>" class="img-responsive img-curve">
                    <?php
                }
                ?>
            </div>

            <div class="food-menu-desc">
                <h2 class="text-center text-black"><?php echo $title; ?></h2>
                <p class="food-price"><?php echo $description; ?></p>
                <h3>RM<?php echo $price; ?></h3>
                <input type="hidden" name="food_id" value="<?php echo $cake_id; ?>">
                <input type="hidden" name="price" value="<?php echo $price; ?>">
                <input type="hidden" name="is_cake" value="1">
                <input type="hidden" name="is_custom" value="1">
                <input type="hidden" name="toppings" id="toppings">

                <div class="order-label">Base Colour</div>
<select name="baseColour" class="input-responsive w3-border" required>
    <option value="White">White</option>
    <option value="Red">Red</option>
    <option value="Blue">Blue</option>
    <option value="Green">Green</option>
    <option value="Yellow">Yellow</option>
    <option value="Purple">Purple</option>
    <option value="Pink">Pink</option>
    <option value="Orange">Orange</option>
    <option value="Brown">Brown</option>
    <option value="Black">Black</option>
    <option value="Silver">Silver</option>
    <option value="Gold">Gold</option>
</select>

<div class="order-label">Decoration Colour</div>
<select name="decoColour" class="input-responsive w3-border" required>
    <option value="White">White</option>
    <option value="Red">Red</option>
    <option value="Blue">Blue</option>
    <option value="Green">Green</option>
    <option value="Yellow">Yellow</option>
    <option value="Purple">Purple</option>
    <option value="Pink">Pink</option>
    <option value="Orange">Orange</option>
    <option value="Brown">Brown</option>
    <option value="Black">Black</option>
    <option value="Silver">Silver</option>
    <option value="Gold">Gold</option>
</select>

                <div class="order-label">Toppings</div>
                <div class="custom-option">
                    <?php
                    $sql2 = "SELECT * FROM `option`";
                    $res2 = mysqli_query($conn, $sql2);
                    while ($row2 = mysqli_fetch_assoc($res2)) {
                        $option_id = $row2['id'];
                        $option_name = $row2['optionName'];
                        $option_desc = $row2['optionDesc'];
                        $option_img = $row2['optionImg'];
                    ?>
                    <div class="food-menu2-img">
                        <img src="<?php echo SITEURL; ?>images/options/<?php echo $option_img; ?>" alt="<?php echo $option_name; ?>" class="img-responsive img-curve">
                    </div>

                    <div class="food-menu2-desc">
                        <h3><?php echo $option_name; ?></h3>
                        <p class="food-price"><?php echo $option_desc; ?></p>
                        <input type="checkbox" name="custom_options[]" value="<?php echo $option_name; ?>">
                    </div>
                    <div class="clearfix"></div>
                    <?php } ?>
                </div>

                <div class="order-label">Cream Writing</div>
                <input type="text" name="creamWrite" class="input-responsive w3-border">

                <div class="order-label">Quantity</div>
                <input type="number" name="qty" class="input-responsive w3-border" value="1" min="1" required>

                <input type="submit" name="addToCart" value="Add To Cart" class="w3-button w3-round-large w3-green">
            </div>
        </fieldset>
    </form>
</div>

<script>
function concatenateToppings() {
    let toppings = [];
    document.querySelectorAll('input[name="custom_options[]"]:checked').forEach(option => {
        toppings.push(option.value);
    });
    document.getElementById('toppings').value = toppings.join(' + ');
}
</script>

<?php include('partials-front/footer.php'); ?>
