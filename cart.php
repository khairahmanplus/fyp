<?php
include('partials-front/menu.php');
if (isset($_GET['custom_id'])) {
    $custom_id = $_GET['custom_id'];
    unset($_GET['custom_id']);
    $sql = "SELECT * FROM customization WHERE id=$custom_id";
    $res = mysqli_query($conn, $sql);
    $count = mysqli_num_rows($res);
    
    if ($count == 1) {
        $row = mysqli_fetch_assoc($res);
        $cust_id = $row['id'];
        $custom_food_id = $row['foodId'];
        $baseColour = $row['baseColour'];
        $decoColour = $row['decoColour'];
        $toppings = $row['toppings'];
        $creamWrite = $row['creamWrite'];
    } else {
        header('location:' . SITEURL);
        exit;
    }


    $cart_food_id = $custom_food_id;
    $qty = 1;
    $is_cake = 1;
    $is_custom = 1;
    if ($is_cake) {
        $custom_desc = "Default decoration";
    }
    if ($is_custom) {
        $cust_username = $_SESSION['username'];
        $cust_sql = "SELECT id FROM customers WHERE username='$cust_username'";
        $cust_res = mysqli_query($conn, $cust_sql);
        if (mysqli_num_rows($cust_res) == 1) {
            $cust_row = mysqli_fetch_assoc($cust_res);
            $cust_id = $cust_row['id'];
        } else {
            header('location:' . SITEURL);
            exit;
        }


        //get price
        $price_sql = "SELECT price FROM food WHERE id = $custom_food_id";
        $price_res = mysqli_query($conn, $price_sql);
    
        if ($price_res && mysqli_num_rows($price_res) == 1) {
            $price_row = mysqli_fetch_assoc($price_res);
            $price = $price_row['price'];
        } else {
            header('location:' . SITEURL);
            exit;
                }
            
        $custom_desc = '(Base: '.$baseColour.', Deco: '.$decoColour.'';
        if (!($toppings === null || trim($toppings) === '')){
            $custom_desc .= ', Topping: '.$toppings.'';
        }
        if (!($creamWrite === null || trim($creamWrite) === '')){
            $custom_desc .= ', Writing: '.$creamWrite.'';
        }
        $custom_desc .= ')';
    }

    if (!isset($_SESSION['cart']) || !is_array($_SESSION['cart'])) {
        $_SESSION['cart'] = array();
    }
    $cart_row_id = count($_SESSION['cart']);
    $_SESSION['cart'][$cart_row_id] = array(
        'food_id' => $cart_food_id,
        'quantity' => $qty,
        'custom_desc' => $custom_desc
    );
    header('location:' . SITEURL.'cart.php');


} 
if (isset($_POST['addToCart'])) {
    unset($_POST['addToCart']);

    $cart_food_id = isset($_POST['food_id']) ? $_POST['food_id'] : $_POST['id'];
    $qty = $_POST['qty'];
    $is_cake = isset($_POST['is_cake']) ? $_POST['is_cake'] : false;
    $is_custom = isset($_POST['is_custom']) ? $_POST['is_custom'] : false;
    if ($is_cake) {
        $custom_desc = "Default decoration";
    }
    if ($is_custom) {
        $cust_username = $_SESSION['username'];
        $cust_sql = "SELECT id FROM customers WHERE username='$cust_username'";
        $cust_res = mysqli_query($conn, $cust_sql);
        if (mysqli_num_rows($cust_res) == 1) {
            $cust_row = mysqli_fetch_assoc($cust_res);
            $cust_id = $cust_row['id'];
        } else {
            header('location:' . SITEURL);
            exit;
        }

        $custom_food_id = $_POST['food_id'];
        $baseColour = $_POST['baseColour'];
        $decoColour = $_POST['decoColour'];
        $toppings = $_POST['toppings'];
        $creamWrite = $_POST['creamWrite'];
        $price = $_POST['price'];

        $stmt = $conn->prepare("INSERT INTO customization (foodId, custId, baseColour, decoColour, toppings, creamWrite) VALUES (?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("iissss", $custom_food_id, $cust_id, $baseColour, $decoColour, $toppings, $creamWrite);
        $stmt->execute();

        $custom_desc = '(Base: '.$baseColour.', Deco: '.$decoColour.'';
        if (!($toppings === null || trim($toppings) === '')){
            $custom_desc .= ', Topping: '.$toppings.'';
        }
        if (!($creamWrite === null || trim($creamWrite) === '')){
            $custom_desc .= ', Writing: '.$creamWrite.'';
        }
        $custom_desc .= ')';
        $customization_id = $stmt->insert_id;
        $stmt->close();
    }

    if (!isset($_SESSION['cart']) || !is_array($_SESSION['cart'])) {
        $_SESSION['cart'] = array();
    }

    $item_combined = false;

    if (!$is_cake) {
        $custom_desc = "";
        foreach ($_SESSION['cart'] as $cart_row_id => $cart_item) {
            if ($cart_item['food_id'] == $cart_food_id) {
                $_SESSION['cart'][$cart_row_id]['quantity'] += $qty;
                $item_combined = true;
                break;
            }
        }
    }

    if (!$item_combined) {
        $cart_row_id = count($_SESSION['cart']);
        $_SESSION['cart'][$cart_row_id] = array(
            'food_id' => $cart_food_id,
            'quantity' => $qty,
            'custom_desc' => $custom_desc
        );
    }
}

if (isset($_GET['remove']) && is_numeric($_GET['remove']) && isset($_SESSION['cart'][$_GET['remove']])) {
    unset($_SESSION['cart'][$_GET['remove']]);
}

if (isset($_POST['submit'])) {
    unset($_GET['custom_id']);
    $foods = "";
    $subtotal = $_POST['total'];
    $fee = $_POST['fee'];
    
    $couponCode = $_POST['coupon_code'];
    $discount = 0.0;

    $sql = "SELECT * FROM coupon WHERE couponCode = '$couponCode' AND isActive = 1";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        $discount = $row['discountValue'];
    } else {
        $discount = 0;
    }

    $discountedSubtotal = $subtotal - ($subtotal * $discount);
    $total = $discountedSubtotal + $fee;

    foreach ($_SESSION['cart'] as $cart_row_id => $cart_item) {
        $food_id = $cart_item['food_id'];
        $quantity = $cart_item['quantity'];
        $custom_desc = $cart_item['custom_desc'];

        if (is_numeric($food_id)) {
            $sql = "SELECT * FROM food WHERE id=$food_id";
            $res = mysqli_query($conn, $sql);
            $count = mysqli_num_rows($res);
            
            if ($count == 1) {
                $row = mysqli_fetch_assoc($res);
                $foods .= "<br>" . $row['title'] . " ".$custom_desc." (RM" . $row['price'] . " x " . $quantity . " = RM" . number_format((float)($quantity * $row['price']), 2, '.', '') . ")";
            }
        }
    }
    $cust_id = $_POST['cust_id'];
    $foods .= "<br><br>Subtotal: RM" . number_format((float)$subtotal, 2, '.', '') . "<br>Subtotal after discount: RM" . number_format((float)$discountedSubtotal, 2, '.', '') . "<br>Delivery fee: RM" . number_format((float)$fee, 2, '.', '') . "<br>Total: RM" . number_format((float)$total, 2, '.', '');

    $order_date = date("Y-m-d h:i:sa"); // Order Date
    $delivery_date = $_POST['delivery_date']; // delivery Date
    $status = "Ordered";  // Ordered, On Delivery, Delivered, Cancelled
    $remark = $_POST['remark'];
    $payment = $_POST['payment'];
    $customer_name = $_POST['full-name'];
    $customer_contact = $_POST['contact'];
    $customer_email = $_POST['email'];
    $customer_address = $_POST['address'];
    
    if ($fee == 10){
        $delivery = 1;
    } else {
        $delivery = 0;
    }
    
    // Save the Order in Database
    // Create SQL to save the data
    $sql2 = "INSERT INTO orders SET
        id = NULL,
        custId = $cust_id,
        food = '$foods',
        total = $total,
        remark = '$remark',
        payment = '$payment',
        delivery = '$delivery',
        order_date = '$order_date',
        delivery_date = '$delivery_date',
        status = '$status',
        customer_name = '$customer_name',
        customer_contact = '$customer_contact',
        customer_email = '$customer_email',
        customer_address = '$customer_address',
        discount = '$discount'  -- Adding the discount value to the orders table
    ";

    // Execute the Query
    $res2 = mysqli_query($conn, $sql2);
    $order_id = mysqli_insert_id($conn);
    
    // Check whether query executed successfully or not
    if($res2 == true) {
        // Clear session
        unset($_SESSION['cart']);
        // Query Executed and Order Saved
        $_SESSION['order'] = "<div class='success text-center'>Food Ordered Successfully. Your tracking number is $order_id. <button id='pdfButton'><b>Click here to print receipt</b></button></div><div id='generatePDF'><fieldset style='max-width:600px;margin: auto;'><h4 style='text-align:center'>PAYMENT RECEIPT⠀⠀⠀⠀⠀⠀⠀⠀Auntie Jue Home Bakes</h4> 
        <table style='width:100%;border: 1px solid black'>
        <tr>
        <td>Bill to: $customer_name</td>
        <td style='text-align:right'>Receipt No.: $order_id</td>
        </tr>
        <tr>
        <td>Payment method: $payment</td>
        <td style='text-align:right'>Order date and time: $order_date</td>
        </tr>
        </table><br>
        <div style='text-align:right'>Item (Price x Quantity = Total)<br>$foods</div><div style='text-align:center'><br>Thank you for your business!</div></fieldset></div>";
        header('location:'.SITEURL);
    } else {
        // Failed to Save Order
        $_SESSION['order'] = "<div class='error text-center'>Failed to Order Food.</div>";
        header('location:'.SITEURL);
    }
}
?>
<div class="w3-container w3-padding-32 w3-light-gray">  
    <h2 class="text-center text-black">Cart and Confirmation</h2>
    <?php if(isset($_SESSION['cart'])):?>
    <form name="orderCart"  action="" method="POST" >
    <div class="w3-row-padding " style="max-width:1200px;margin: auto;">
          <div class="w3-half w3-hover-opacity"><div id="t9">
        <h3>Selected Foods</h3>
        <?php  
            $total = (float)0;
            foreach ($_SESSION['cart'] as $cart_row_id => $cart_item) {
                $food_id = $cart_item['food_id'];
                $quantity = $cart_item['quantity'];
                $custom_desc = $cart_item['custom_desc'];
                if(is_numeric($food_id)){
                    $sql = "SELECT * FROM food WHERE id=$food_id";
                    $res = mysqli_query($conn, $sql);
                    $count = mysqli_num_rows($res);
                    if($count == 1) {
                        $row = mysqli_fetch_assoc($res);
                        $title = $row['title'];
                        $price = $row['price'];
                        $image_name = $row['image_name'];
                        $total = $total + ($price * $quantity);
                    }
                echo '<div class="food-menu2-img">';
                    echo '<img src="'.SITEURL.'images/food/'.$image_name.'" alt="'.$title.'" class="img-responsive img-curve">';
                echo '</div>
                <div class="food-menu2-desc">
                    <h3>'.$title.'</h3>
                    <p>'.$custom_desc.'</p>
                    <p class="food-price" id="price'.$cart_row_id.'">'.$price.'</p>
                    <div class="order-label">Quantity</div>
                    <input type="hidden" value="'.$quantity.'" name="qty'.$cart_row_id.'">
                    <input type="number" name="'.$cart_row_id.'" class="input-responsive" value="'.$quantity.'" min="1" onchange="price()" id="'.$cart_row_id.'" required>
                    <a href="cart.php?remove='.$cart_row_id.'" class="w3-text-red">Remove</a>              
                </div><div class="clearfix"></div>';
                }
            }
        ?>
        <h2 id="totalDisplay">Cart total: RM<?php echo $total;?></h2>
        <input type="hidden" name="total" value="<?php echo $total;?>">
    </div></div>

    <div class="w3-half w3-hover-opacity"><div id="t10">
    <h3>Delivery Details</h3>   
                <?php 
if (isset($_SESSION['username'])){
      
                    //Get the DEtails of the SElected Food
                    $sql = "SELECT * FROM customers WHERE username = '" . $_SESSION['username'] . "'";
                    //Execute the Query
                    $res = mysqli_query($conn, $sql);
                    //Count the rows
                    $count = mysqli_num_rows($res);
                    //CHeck whether the data is available or not
                    if($count==1)
                    {
                        //WE Have DAta
                        //GEt the Data from Database
                        $row = mysqli_fetch_assoc($res);
                        $cust_id = $row['id'];
                        $full_name = $row['full_name'];
                        $contact_no = $row['contact_no'];
                        $address = $row['address'];
                        $email = $row['email'];
                    }
    


echo '
                <input type="hidden" name="cust_id" value="'.$cust_id.'">
                             
                <div class="order-label">Remark</div>
                <input type="text" name="remark" placeholder="e.g. Use no cream for cupcake" class="input-responsive">

                <div class="order-label">Full Name</div>
                <input type="text" name="full-name" value="'.$full_name.'" placeholder="e.g. Ali bin Musa" class="input-responsive" required>

                <div class="order-label">Phone Number</div>
                <input type="tel" name="contact" value="'.$contact_no.'" placeholder="e.g. 012-3456789" class="input-responsive" required>

                <div class="order-label">Email</div>
                <input type="email" name="email" value="'.$email.'" placeholder="e.g. ali@example.com" class="input-responsive" required>

                <div class="order-label">Address</div>
                <textarea name="address" rows="10" placeholder="e.g. Street, City, State" class="input-responsive" required>'.$address.'</textarea>

                <div class="order-label">Payment method</div>
                <select name="payment" id="payment" class="input-responsive w3-border" required onchange="handlePaymentChange()">
                <option value=""></option>
                    <option value="Cash" onfocus="hideCardForm();">Cash</option>
                    <option value="Card" onfocus="showCardForm();">Credit Card / Debit Card</option>
                </select> 
                
                <div id="card" style="display:none">
                <div class="order-label">Billing Address</div>
                <textarea rows="10" placeholder="E.g. Street, City, State" class="input-responsive" >'.$address.'</textarea>
                
                <div class="order-label">Full Name on Card</div>
                <input type="text" value="'.$full_name.'" placeholder="E.g. Ali bin Musa" class="input-responsive" >

                <div class="order-label">Card Number</div>
                <input type="text" class="input-responsive" >

                <div class="order-label">3 Digit Security Code</div>
                <input type="text" class="input-responsive" >

                <div class="order-label">Expiration Date</div>
                <input type="date" class="input-responsive" >

                <div class="input-responsive">
                <input type="radio" id="age1" name="age" value="30">
                <label for="age1">Visa</label><br>
                <input type="radio" id="age2" name="age" value="60">
                <label for="age2">Mastercard</label><br>
                </div>
              
                </div>

                <div class="order-label ">Collection method </div>
                <select name="fee" id="fee" class="input-responsive w3-border" required>
                    <option value=""></option>
                    <option value="10">Delivery (Delivery fee: +RM10)</option>
                    <option value="0">Self-Collect (+RM0)</option>
                </select> 

    <div class="order-label">Delivery Date (leave empty if want to order as soon as possible)</div>
    <input type="date" name="delivery_date" id="delivery_date" class="input-responsive"  min="'.date('Y-m-d', strtotime('+1 day')).'">

                <div class="order-label">Coupon Code</div>
                <input type="text" name="coupon_code" id="coupon_code" placeholder="Coupon code" class="input-responsive">
                <div id="coupon_message"></div>
                <br>
                <input type="submit" name="submit" value="Confirm Order" class="btn btn-primary">
                ';
    }else{
        echo "<p>You are not logged in. <a class='w3-text-green' href='login.php'>Login Here</a></p>";
    }?>
        <?php else:?>
        <fieldset>Your cart is empty. Please start adding foods to your cart.</fieldset>
        <?php endif;?>
</div></div>
        </div>

        </form>
        </div>

        <script>
if ( window.history.replaceState ) {
  window.history.replaceState( null, null, window.location.href );
}

function price(){
    var total = 0;
    var inputs = document.querySelectorAll('input[type=number]');
    inputs.forEach(function(input) {
        var price = parseFloat(document.getElementById('price' + input.id).innerHTML);
        var quantity = input.value;
        total += price * quantity;
    });
    document.getElementsByName('total')[0].value = total.toFixed(2);
    document.getElementById('totalDisplay').innerHTML = 'Cart total: RM' + total.toFixed(2);
}


function handlePaymentChange() {
        var paymentMethod = document.getElementById("payment").value;
        if (paymentMethod === "Card") {
            showCardForm();
        } else {
            hideCardForm();
        }
    }

    function showCardForm() {
        document.getElementById('card').style.display = "";
    }

    function hideCardForm() {
        document.getElementById('card').style.display = "none";
    }
    
// Add event listener to the coupon code input field
document.getElementById("coupon_code").addEventListener("keyup", function() {
    // Get the value of the coupon code input field
    var couponCode = this.value;
    
    // Create an XMLHttpRequest object
    var xhr = new XMLHttpRequest();
    
    // Define the request
    xhr.open("POST", "coupon-check.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    
    // Define what happens on response
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // Update the coupon message span with the response text
            document.getElementById("coupon_message").innerHTML = xhr.responseText;
        }
    };
    
    // Send the request with the coupon code as data
    xhr.send("coupon_code=" + couponCode);
});
    // Example JavaScript for validating delivery date against order date
    document.getElementById('delivery_date').addEventListener('change', function() {
        var deliveryDate = new Date(this.value);
        var orderDate = new Date(); // Get the order date dynamically if needed
        orderDate.setHours(0, 0, 0, 0); // Reset time to start of the day
        
        if (deliveryDate <= orderDate) {
            alert('Delivery date must be after the order date.');
            this.value = ''; // Clear the date input if invalid
        }
    });


</script>
<?php include('partials-front/footer.php'); ?>
