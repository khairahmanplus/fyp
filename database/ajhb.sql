-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 22, 2024 at 02:54 PM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ajhb`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(10) UNSIGNED NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `full_name`, `username`, `password`) VALUES
(12, 'Administrator', 'admin', 'E10ADC3949BA59ABBE56E057F20F883E');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `featured` varchar(10) NOT NULL,
  `active` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `title`, `image_name`, `featured`, `active`) VALUES
(1, 'Cakes', 'Food_Category_569.jpg', 'Yes', 'Yes'),
(2, 'Cupcakes', 'Food_Category_253.jpg', 'Yes', 'Yes'),
(3, 'Cookies', 'Food_Category_319.jpg', 'Yes', 'Yes'),
(4, 'Pastries', 'Food_Category_421.jpg', 'Yes', 'Yes'),
(5, 'Breads', 'Food_Category_480.jpg', 'Yes', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `coupon`
--

CREATE TABLE `coupon` (
  `couponCode` varchar(50) NOT NULL,
  `discountValue` decimal(3,2) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `coupon`
--

INSERT INTO `coupon` (`couponCode`, `discountValue`, `isActive`) VALUES
('test', 0.50, 1),
('test2', 0.50, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) NOT NULL,
  `full_name` varchar(150) NOT NULL,
  `contact_no` varchar(20) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(150) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `full_name`, `contact_no`, `address`, `email`, `username`, `password`) VALUES
(1, 'Misbun Sidek', '0123456789', 'No 30, Jalan Kelisa, Parit Raja, Johor', 'misbun@gmail.com', 'misbun', '4e77d35247fccbce2045a04afe221525'),
(3, 'Azwan Ali', '555555', 'ffff', 'aa@gmail.com', 'aa', '698d51a19d8a121ce581499d7b701668'),
(4, 'Ali bin Musa', '011-1111111', 'No 1, Jln A, Tmn A, Parit Raja,\r\nBatu Pahat, Johor', 'a@a.com', 'ali', '3dbe00a167653a1aaee01d93e77e730e'),
(5, 'Test', '011-22222222', 'No 2, Jln 2, Tmn 22, Parit Raja,\r\nBatu Pahat, Johor', '2@2.com', '2', 'bae5e3208a3c700e3db642b6631e95b9');

-- --------------------------------------------------------

--
-- Table structure for table `customization`
--

CREATE TABLE `customization` (
  `id` int(11) NOT NULL,
  `foodId` int(11) NOT NULL,
  `custId` int(11) NOT NULL,
  `baseColour` varchar(64) NOT NULL,
  `decoColour` varchar(64) NOT NULL,
  `toppings` varchar(255) DEFAULT NULL,
  `creamWrite` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `customization`
--

INSERT INTO `customization` (`id`, `foodId`, `custId`, `baseColour`, `decoColour`, `toppings`, `creamWrite`) VALUES
(1, 5, 4, 'Red', 'Red', 'Sprinkles + Edible flower', 'Happy bday bro'),
(17, 5, 4, 'Red', 'Red', 'Sprinkles + Edible flower', 'wwww'),
(18, 9, 4, 'Red', 'Red', 'Sprinkles + Edible flower', 'hb bro'),
(19, 4, 4, 'Red', 'Red', 'Sprinkles', 'sss'),
(20, 4, 4, 'Yellow', 'Green', 'Sprinkles + Edible flower', 'happy grad'),
(21, 9, 4, 'Blue', 'Red', 'Sprinkles', 'rrr'),
(22, 9, 4, 'Blue', 'Red', 'Sprinkles', 'rrr'),
(24, 5, 4, 'Red', 'Red', 'Edible flower', 'wa');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `feedbackId` int(11) NOT NULL,
  `custId` int(11) DEFAULT NULL,
  `feedbackMessage` varchar(255) DEFAULT NULL,
  `timeSubmitted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`feedbackId`, `custId`, `feedbackMessage`, `timeSubmitted`) VALUES
(1, 4, 'test', '2024-06-12 21:54:49'),
(2, 4, ' ', '2024-06-12 21:54:56'),
(3, 4, 'test', '2024-06-12 22:43:46'),
(4, 4, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor ', '2024-06-13 07:01:13');

-- --------------------------------------------------------

--
-- Table structure for table `food`
--

CREATE TABLE `food` (
  `id` int(10) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `featured` varchar(10) NOT NULL,
  `active` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `food`
--

INSERT INTO `food` (`id`, `title`, `description`, `price`, `image_name`, `category_id`, `featured`, `active`) VALUES
(4, 'Carrot Cake', 'Diameter: 8 inch<br>Height: 5 inch<br>Toppings: Cream cheese & crushed nuts<br>Serves: 8-12 people', 80.00, 'Food-Name-311.jpg', 1, 'No', 'Yes'),
(5, 'Cheesecake', 'Diameter: 8 inch\r\n<br>\r\nHeight: 5 inch\r\n<br>\r\nToppings: Cream cheese & strawberries\r\n<br>\r\nServes: 8-12 people', 80.00, 'Food-Name-8847.jpg', 1, 'Yes', 'Yes'),
(9, 'Chocolate Moist Cake', 'Diameter: 6 inch\r\n<br>\r\nHeight: 5 inch\r\n<br>\r\nToppings: Chocolate buttercream & chocolate pieces\r\n<br>\r\nServes: 8-12 people', 60.00, 'Food-Name-5741.jpg', 1, 'Yes', 'Yes'),
(10, 'Chocolate Cupcakes - 15 pcs', 'Diameter: 4.4 cm (Base) 5.5 cm (Top)\r\n<br>\r\nHeight: 3.4 cm\r\n<br>\r\nToppings: Chocolate buttercream\r\n<br>\r\nServes: 15 people', 60.00, 'Food-Name-256.jpg', 2, 'Yes', 'Yes'),
(11, 'Red Velvet Cupcakes - 15 pcs', 'Diameter: 4.4 cm (Base) 5.5 cm (Top)\r\n<br>\r\nHeight: 3.4 cm\r\n<br>\r\nToppings: Cream cheese & red velvet crumbs\r\n<br>\r\nServes: 15 people', 67.00, 'Food-Name-2831.jpg', 2, 'No', 'Yes'),
(12, 'Strawberry Cupcakes - 15 pcs', 'Diameter: 4.4 cm (Base) 5.5 cm (Top)\r\n<br>\r\nHeight: 3.4 cm\r\n<br>\r\nToppings: Strawberry buttercream & half a strawberry\r\n<br>\r\nServes: 15 people\r\n\r\n', 60.00, 'Food-Name-6475.jpg', 2, 'No', 'Yes'),
(13, 'Classic Chocolate Chip Cookies', 'Net weight: 500 g\r\n<br>\r\nTotal servings: 24 pieces\r\n<br>\r\nFillings: Chocolate chips\r\n<br>\r\nServes: 8-24 people', 33.00, 'Food-Name-9432.jpg', 3, 'No', 'Yes'),
(14, 'Nutella Chocolate Chip Cookies', 'Net weight: 500 g\r\n<br>\r\nTotal servings: 24 pieces\r\n<br>\r\nFillings: Chocolate chips & Nutella\r\n<br>\r\nServes: 8-24 people', 40.00, 'Food-Name-7443.jpg', 3, 'Yes', 'Yes'),
(15, 'Raisin Oatmeal Cookies', 'Net weight: 500 g\r\n<br>\r\nTotal servings: 24 pieces\r\n<br>\r\nFillings: Raisins\r\n<br>\r\nServes: 8-24 people', 33.00, 'Food-Name-660.jpg', 3, 'No', 'Yes'),
(16, 'Cheese Tart - 6 pcs', 'Diameter: 3 cm (Base) 5.5 cm (Top)\r\n<br>\r\nHeight: 1.5 cm\r\n<br>\r\nFillings: Cream cheese\r\n<br>\r\nServes: 6 people', 30.00, 'Food-Name-7073.jpg', 4, 'Yes', 'Yes'),
(17, 'Pavlova', 'Diameter: 6 inch\r\n<br>\r\nHeight: 3 inch\r\n<br>\r\nFillings: Fresh fruits and whipped cream\r\n<br>\r\nServes: 3-8 people', 45.00, 'Food-Name-8323.jpg', 4, 'No', 'Yes'),
(18, 'Pineapple Tart - 45 pcs', 'Net weight: 500 g\r\n<br>\r\nTotal servings: 45 pieces\r\n<br>\r\nFillings: Pineapple jam\r\n<br>\r\nServes: 8-24 people', 30.00, 'Food-Name-839.jpg', 4, 'No', 'Yes'),
(19, 'Cream Cheese Garlic Bun', 'Net weight: 144g\r\n<br>\r\nTotal servings: 1 piece\r\n<br>\r\nFillings: Cream cheese & garlic\r\n<br>\r\nServes: 1-2 people', 6.00, 'Food-Name-5826.jpg', 5, 'Yes', 'Yes'),
(20, 'Garlic Bread', 'Net weight: 324 g\r\n<br>\r\nTotal servings: 1 loaf (6 slices)\r\n<br>\r\nFillings: Garlic butter\r\n<br>\r\nServes: 3-6 people', 7.00, 'Food-Name-6720.jpg', 5, 'No', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `option`
--

CREATE TABLE `option` (
  `id` int(11) NOT NULL,
  `optionName` varchar(50) NOT NULL,
  `optionDesc` varchar(150) DEFAULT NULL,
  `optionImg` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `option`
--

INSERT INTO `option` (`id`, `optionName`, `optionDesc`, `optionImg`) VALUES
(1, 'Sprinkles', 'Colourful sprinkles for topping', 'Option_453.jpg'),
(2, 'Edible flower', 'Flower made by flour', 'Option_229.avif');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `custId` int(10) NOT NULL,
  `food` text NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `delivery` tinyint(1) NOT NULL,
  `payment` varchar(30) NOT NULL,
  `order_date` datetime NOT NULL,
  `delivery_date` date DEFAULT NULL,
  `status` varchar(50) NOT NULL,
  `customer_name` varchar(150) NOT NULL,
  `customer_contact` varchar(20) NOT NULL,
  `customer_email` varchar(150) NOT NULL,
  `customer_address` varchar(255) NOT NULL,
  `discount` decimal(3,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `custId`, `food`, `total`, `remark`, `delivery`, `payment`, `order_date`, `delivery_date`, `status`, `customer_name`, `customer_contact`, `customer_email`, `customer_address`, `discount`) VALUES
(45, 4, '<br>Carrot Cake (RM80.00 x 1 = RM80.00)<br>Cheesecake (RM80.00 x 1 = RM80.00)<br>Red Velvet Cupcakes - 15 pcs (RM67.00 x 2 = RM134.00)<br>Nutella Chocolate Chip Cookies (RM40.00 x 2 = RM80.00)<br><br>Subtotal: RM374.00<br>Delivery fee: RM10.00<br>Total: RM384.00', 384.00, 'no strawberries', 1, 'Cash', '2023-01-07 04:13:30', NULL, 'Ready', 'Azwan Ali', '555555', 'aa@gmail.com', 'ffff', 0.00),
(46, 4, '<br>Chocolate Moist Cake (RM60.00 x 1 = RM60.00)<br><br>Subtotal: RM60.00<br>Delivery fee: RM0.00<br>Total: RM60.00', 60.00, 'cut into 4 pieces', 0, 'Online Bank', '2023-01-07 04:17:06', NULL, 'On Delivery', 'Azwan Ali', '555555', 'aa@gmail.com', 'ffff', 0.00),
(47, 4, '<br>Carrot Cake (RM80.00 x 1 = RM80.00)<br>Chocolate Moist Cake (RM60.00 x 4 = RM240.00)<br><br>Subtotal: RM320.00<br>Delivery fee: RM10.00<br>Total: RM330.00', 330.00, '', 1, 'Cash', '2023-01-07 05:45:37', NULL, 'Ordered', 'Azwan Ali', '555555', 'aa@gmail.com', 'ffff', 0.00),
(48, 5, '<br>Cheese Tart - 6 pcs (RM30.00 x 1 = RM30.00)<br><br>Subtotal: RM30.00<br>Delivery fee: RM0.00<br>Total: RM30.00', 30.00, '', 0, 'Cash', '2024-06-12 02:26:52', NULL, 'Ordered', 'Ali bin Musa', '011-1111111', 'a@a.com', 'No 1, Jln A, Tmn A, Parit Raja,\r\nBatu Pahat, Johor', 0.00),
(49, 5, '<br>Classic Chocolate Chip Cookies (RM33.00 x 1 = RM33.00)<br><br>Subtotal: RM33.00<br>Subtotal after discount: RM16.50<br>Delivery fee: RM0.00<br>Total: RM16.50', 16.50, '', 0, 'Cash', '2024-06-12 04:54:37', NULL, 'Ordered', 'Ali bin Musa', '011-1111111', 'a@a.com', 'No 1, Jln A, Tmn A, Parit Raja,\r\nBatu Pahat, Johor', 0.50),
(50, 4, '<br>Cream Cheese Garlic Bun (RM6.00 x 1 = RM6.00)<br><br>Subtotal: RM6.00<br>Subtotal after discount: RM6.00<br>Delivery fee: RM10.00<br>Total: RM16.00', 16.00, '', 1, 'Cash', '2024-06-12 04:58:40', NULL, 'Processing', 'Ali bin Musa', '011-1111111', 'a@a.com', 'No 1, Jln A, Tmn A, Parit Raja,\r\nBatu Pahat, Johor', 0.00),
(51, 4, '<br>Red Velvet Cupcakes - 15 pcs (RM67.00 x 1 = RM67.00)<br>Cheese Tart - 6 pcs (RM30.00 x 2 = RM60.00)<br><br>Subtotal: RM127.00<br>Subtotal after discount: RM63.50<br>Delivery fee: RM10.00<br>Total: RM73.50', 73.50, 'No cream for the cupcakes', 1, 'Cash', '2024-06-12 08:13:50', NULL, 'Ordered', 'Ali bin Musa', '011-1111111', 'a@a.com', 'No 1, Jln A, Tmn A, Parit Raja,\r\nBatu Pahat, Johor', 0.50),
(52, 5, '<br>Carrot Cake (RM80.00 x 2 = RM160.00)<br><br>Subtotal: RM160.00<br>Subtotal after discount: RM160.00<br>Delivery fee: RM5.00<br>Total: RM165.00', 165.00, '', 0, '0', '0000-00-00 00:00:00', NULL, 'Ordered', 'a', 'a', 'a@a.1', 'a', 0.00),
(53, 4, '<br>Chocolate Moist Cake(Default decoration) (RM60.00 x 1 = RM60.00)<br>Garlic Bread() (RM7.00 x 1 = RM7.00)<br>Carrot Cake(Base: Yellow, Deco: Green, Topping: Sprinkles + Edible flower, Writing: happy grad) (RM80.00 x 1 = RM80.00)<br><br>Subtotal: RM147.00<br>Subtotal after discount: RM73.50<br>Delivery fee: RM0.00<br>Total: RM73.50', 73.50, '', 0, 'Cash', '2024-06-22 01:33:47', NULL, 'Ordered', 'Ali bin Musa', '011-1111111', 'a@a.com', 'No 1, Jln A, Tmn A, Parit Raja,\r\nBatu Pahat, Johor', 0.50),
(54, 4, '<br>Chocolate Moist Cake ((Base: Blue, Deco: Red, Topping: Sprinkles, Writing: rrr)) (RM60.00 x 1 = RM60.00)<br>Chocolate Moist Cake ((Base: Blue, Deco: Red, Topping: Sprinkles, Writing: rrr)) (RM60.00 x 2 = RM120.00)<br><br>Subtotal: RM180.00<br>Subtotal after discount: RM90.00<br>Delivery fee: RM10.00<br>Total: RM100.00', 100.00, '', 1, 'Cash', '2024-06-22 01:35:12', NULL, 'Ordered', 'Ali bin Musa', '011-1111111', 'a@a.com', 'No 1, Jln A, Tmn A, Parit Raja,\r\nBatu Pahat, Johor', 0.50),
(55, 4, '<br>Carrot Cake (Base: Red, Deco: Red, Topping: , Writing: ) (RM80.00 x 1 = RM80.00)<br><br>Subtotal: RM80.00<br>Subtotal after discount: RM80.00<br>Delivery fee: RM10.00<br>Total: RM90.00', 90.00, '', 1, 'Cash', '2024-06-22 01:41:45', NULL, 'Ordered', 'Ali bin Musa', '011-1111111', 'a@a.com', 'No 1, Jln A, Tmn A, Parit Raja,\r\nBatu Pahat, Johor', 0.00),
(56, 4, '<br>Cheesecake (Base: Red, Deco: Red) (RM80.00 x 1 = RM80.00)<br>Cheesecake (Base: Red, Deco: Red, Topping: Edible flower) (RM80.00 x 1 = RM80.00)<br>Carrot Cake (Base: Red, Deco: Red, Writing: i) (RM80.00 x 1 = RM80.00)<br><br>Subtotal: RM240.00<br>Subtotal after discount: RM240.00<br>Delivery fee: RM10.00<br>Total: RM250.00', 250.00, '', 1, 'Cash', '2024-06-22 02:50:37', NULL, 'Cancelled', 'Ali bin Musa', '011-1111111', 'a@a.com', 'No 1, Jln A, Tmn A, Parit Raja,\r\nBatu Pahat, Johor', 0.00),
(57, 4, '<br>Cream Cheese Garlic Bun  (RM6.00 x 1 = RM6.00)<br>Carrot Cake (Base: Red, Deco: Red, Writing: i) (RM80.00 x 1 = RM80.00)<br>Carrot Cake (Base: Blue, Deco: Blue, Topping: Sprinkles + Edible flower) (RM80.00 x 1 = RM80.00)<br>Carrot Cake (Base: Red, Deco: Red, Writing: i) (RM80.00 x 1 = RM80.00)<br>Carrot Cake (Base: Red, Deco: Red, Writing: i) (RM80.00 x 1 = RM80.00)<br><br>Subtotal: RM246.00<br>Subtotal after discount: RM246.00<br>Delivery fee: RM10.00<br>Total: RM256.00', 256.00, '', 1, 'Cash', '2024-06-22 04:34:58', NULL, 'Ordered', 'Ali bin Musa', '011-1111111', 'a@a.com', 'No 1, Jln A, Tmn A, Parit Raja,\r\nBatu Pahat, Johor', 0.00),
(58, 4, '<br>Carrot Cake (Base: Red, Deco: Red, Writing: i) (RM80.00 x 1 = RM80.00)<br>Carrot Cake (Base: Red, Deco: Red, Writing: i) (RM80.00 x 1 = RM80.00)<br>Carrot Cake (Base: Red, Deco: Red, Writing: i) (RM80.00 x 1 = RM80.00)<br><br>Subtotal: RM160.00<br>Subtotal after discount: RM160.00<br>Delivery fee: RM10.00<br>Total: RM170.00', 170.00, '', 1, 'Cash', '2024-06-22 04:35:43', NULL, 'Ordered', 'Ali bin Musa', '011-1111111', 'a@a.com', 'No 1, Jln A, Tmn A, Parit Raja,\r\nBatu Pahat, Johor', 0.00),
(59, 4, '<br>Carrot Cake (Base: Red, Deco: Red, Writing: i) (RM80.00 x 1 = RM80.00)<br>Carrot Cake (Base: Red, Deco: Red, Writing: i) (RM80.00 x 1 = RM80.00)<br><br>Subtotal: RM80.00<br>Subtotal after discount: RM80.00<br>Delivery fee: RM0.00<br>Total: RM80.00', 80.00, '', 0, 'Cash', '2024-06-22 04:37:51', NULL, 'Ordered', 'Ali bin Musa', '011-1111111', 'a@a.com', 'No 1, Jln A, Tmn A, Parit Raja,\r\nBatu Pahat, Johor', 0.00),
(60, 4, '<br>Chocolate Moist Cake (Base: Blue, Deco: Red, Topping: Sprinkles, Writing: rrr) (RM60.00 x 1 = RM60.00)<br>Chocolate Moist Cake (Base: Blue, Deco: Red, Topping: Sprinkles, Writing: rrr) (RM60.00 x 1 = RM60.00)<br><br>Subtotal: RM60.00<br>Subtotal after discount: RM60.00<br>Delivery fee: RM10.00<br>Total: RM70.00', 70.00, '', 1, 'Cash', '2024-06-22 04:38:43', NULL, 'Ordered', 'Ali bin Musa', '011-1111111', 'a@a.com', 'No 1, Jln A, Tmn A, Parit Raja,\r\nBatu Pahat, Johor', 0.00),
(61, 4, '<br>Cheesecake (Base: Red, Deco: Red, Topping: Sprinkles + Edible flower, Writing: Happy bday bro) (RM80.00 x 1 = RM80.00)<br>Cheesecake (Base: Red, Deco: Red, Topping: Sprinkles + Edible flower, Writing: Happy bday bro) (RM80.00 x 1 = RM80.00)<br><br>Subtotal: RM80.00<br>Subtotal after discount: RM80.00<br>Delivery fee: RM10.00<br>Total: RM90.00', 90.00, '', 1, 'Cash', '2024-06-22 04:46:11', NULL, 'Ordered', 'Ali bin Musa', '011-1111111', 'a@a.com', 'No 1, Jln A, Tmn A, Parit Raja,\r\nBatu Pahat, Johor', 0.00),
(62, 4, '<br>Cheesecake (Base: White, Deco: White) (RM80.00 x 1 = RM80.00)<br><br>Subtotal: RM80.00<br>Subtotal after discount: RM80.00<br>Delivery fee: RM10.00<br>Total: RM90.00', 90.00, '', 1, 'Cash', '2024-06-22 04:48:12', NULL, 'Ordered', 'Ali bin Musa', '011-1111111', 'a@a.com', 'No 1, Jln A, Tmn A, Parit Raja,\r\nBatu Pahat, Johor', 0.00),
(63, 4, '<br>Chocolate Moist Cake (Base: Red, Deco: Red, Topping: Edible flower) (RM60.00 x 1 = RM60.00)<br>Chocolate Moist Cake (Base: Red, Deco: Red, Topping: Edible flower) (RM60.00 x 1 = RM60.00)<br><br>Subtotal: RM60.00<br>Subtotal after discount: RM60.00<br>Delivery fee: RM10.00<br>Total: RM70.00', 70.00, '', 1, 'Cash', '2024-06-22 05:00:41', NULL, 'Ordered', 'Ali bin Musa', '011-1111111', 'a@a.com', 'No 1, Jln A, Tmn A, Parit Raja,\r\nBatu Pahat, Johor', 0.00),
(64, 4, '<br>Carrot Cake (Base: Red, Deco: Red) (RM80.00 x 1 = RM80.00)<br>Cheesecake (Base: Red, Deco: Red, Topping: Sprinkles) (RM80.00 x 1 = RM80.00)<br><br>Subtotal: RM160.00<br>Subtotal after discount: RM160.00<br>Delivery fee: RM0.00<br>Total: RM160.00', 160.00, '', 0, 'Cash', '2024-06-22 05:05:01', NULL, 'Ordered', 'Ali bin Musa', '011-1111111', 'a@a.com', 'No 1, Jln A, Tmn A, Parit Raja,\r\nBatu Pahat, Johor', 0.00),
(65, 4, '<br>Carrot Cake (Base: Red, Deco: Red) (RM80.00 x 1 = RM80.00)<br><br>Subtotal: RM80.00<br>Subtotal after discount: RM80.00<br>Delivery fee: RM10.00<br>Total: RM90.00', 90.00, '', 1, 'Cash', '2024-06-22 05:16:21', NULL, 'Ordered', 'Ali bin Musa', '011-1111111', 'a@a.com', 'No 1, Jln A, Tmn A, Parit Raja,\r\nBatu Pahat, Johor', 0.00),
(66, 4, '<br>Chocolate Cupcakes - 15 pcs  (RM60.00 x 1 = RM60.00)<br>Chocolate Moist Cake (Base: Red, Deco: Red, Topping: Sprinkles + Edible flower, Writing: hb bro) (RM60.00 x 1 = RM60.00)<br><br>Subtotal: RM120.00<br>Subtotal after discount: RM120.00<br>Delivery fee: RM10.00<br>Total: RM130.00', 130.00, '', 1, 'Cash', '2024-06-22 06:24:34', NULL, 'Ordered', 'Ali bin Musa', '011-1111111', 'a@a.com', 'No 1, Jln A, Tmn A, Parit Raja,\r\nBatu Pahat, Johor', 0.00),
(67, 4, '<br>Carrot Cake (Base: Red, Deco: Red) (RM80.00 x 1 = RM80.00)<br><br>Subtotal: RM80.00<br>Subtotal after discount: RM80.00<br>Delivery fee: RM10.00<br>Total: RM90.00', 90.00, '', 1, 'Cash', '2024-06-22 06:25:05', '0000-00-00', 'Delivered', 'Ali bin Musa', '011-1111111', 'a@a.com', 'No 1, Jln A, Tmn A, Parit Raja,\r\nBatu Pahat, Johor', 0.00),
(68, 4, '<br>Cheesecake (Base: Red, Deco: Red, Topping: Sprinkles + Edible flower, Writing: Happy bday bro) (RM80.00 x 1 = RM80.00)<br><br>Subtotal: RM80.00<br>Subtotal after discount: RM80.00<br>Delivery fee: RM10.00<br>Total: RM90.00', 90.00, '', 1, 'Cash', '2024-06-22 06:26:01', '2024-06-23', 'Delivered', 'Ali bin Musa', '011-1111111', 'a@a.com', 'No 1, Jln A, Tmn A, Parit Raja,\r\nBatu Pahat, Johor', 0.00),
(69, 4, '<br>Carrot Cake (Base: Red, Deco: Red) (RM80.00 x 1 = RM80.00)<br><br>Subtotal: RM80.00<br>Subtotal after discount: RM80.00<br>Delivery fee: RM10.00<br>Total: RM90.00', 90.00, '', 1, 'Cash', '2024-06-22 06:27:55', '2024-06-23', 'Delivered', 'Ali bin Musa', '011-1111111', 'a@a.com', 'No 1, Jln A, Tmn A, Parit Raja,\r\nBatu Pahat, Johor', 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id` int(11) NOT NULL,
  `custId` int(11) DEFAULT NULL,
  `foodId` int(11) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `reviewMessage` varchar(255) DEFAULT NULL,
  `timeSubmitted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id`, `custId`, `foodId`, `rating`, `reviewMessage`, `timeSubmitted`) VALUES
(6, 4, 12, 8, 'good', '2024-06-13 06:08:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`couponCode`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customization`
--
ALTER TABLE `customization`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foodId` (`foodId`),
  ADD KEY `custId` (`custId`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`feedbackId`),
  ADD KEY `custId` (`custId`);

--
-- Indexes for table `food`
--
ALTER TABLE `food`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `option`
--
ALTER TABLE `option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `review_ibfk_1` (`custId`),
  ADD KEY `review_ibfk_2` (`foodId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `customization`
--
ALTER TABLE `customization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `feedbackId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `food`
--
ALTER TABLE `food`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `option`
--
ALTER TABLE `option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `customization`
--
ALTER TABLE `customization`
  ADD CONSTRAINT `customization_ibfk_1` FOREIGN KEY (`foodId`) REFERENCES `food` (`id`),
  ADD CONSTRAINT `customization_ibfk_2` FOREIGN KEY (`custId`) REFERENCES `customers` (`id`);

--
-- Constraints for table `feedback`
--
ALTER TABLE `feedback`
  ADD CONSTRAINT `feedback_ibfk_1` FOREIGN KEY (`custId`) REFERENCES `customers` (`id`);

--
-- Constraints for table `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `review_ibfk_1` FOREIGN KEY (`custId`) REFERENCES `customers` (`id`),
  ADD CONSTRAINT `review_ibfk_2` FOREIGN KEY (`foodId`) REFERENCES `food` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
