<?php include('config/constants.php'); 
if(isset($_POST['coupon_code'])) {
    $couponCode = mysqli_real_escape_string($conn, $_POST['coupon_code']);
    $sql = "SELECT * FROM coupon WHERE couponCode = '$couponCode' AND isActive = 1";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        
        $row = mysqli_fetch_assoc($result);
        $discount= $row['discountValue'];
        $discountDisplay = $discount*100;
        echo "<span class='w3-text-light-green'>Coupon code is valid! Discount: -$discountDisplay%</span>";
    } else {
        echo "<span class='w3-text-red'>Coupon code is not valid or inactive.</span>";
    }
}
?>