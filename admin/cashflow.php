<?php 
include('admin-sections/header.php'); 

// Initialize variables
$orders = [];
$total_profit = 0;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // Handle form submission for generating report
    if (isset($_POST['generate_report']) || isset($_POST['export_csv'])) {
        $timeframe = $_POST['timeframe'];
        $start_date = isset($_POST['start_date']) ? $_POST['start_date'] : null;
        $end_date = isset($_POST['end_date']) ? $_POST['end_date'] : null;

        // Determine date range based on selected timeframe
        switch($timeframe) {
            case 'today':
                $start_date = date('Y-m-d 00:00:00');
                $end_date = date('Y-m-d 23:59:59');
                break;
            case 'week':
                $start_date = date('Y-m-d 00:00:00', strtotime('-1 week'));
                $end_date = date('Y-m-d 23:59:59');
                break;
            case 'month':
                $start_date = date('Y-m-d 00:00:00', strtotime('-1 month'));
                $end_date = date('Y-m-d 23:59:59');
                break;
            case '6months':
                $start_date = date('Y-m-d 00:00:00', strtotime('-6 months'));
                $end_date = date('Y-m-d 23:59:59');
                break;
            case 'year':
                $start_date = date('Y-m-d 00:00:00', strtotime('-1 year'));
                $end_date = date('Y-m-d 23:59:59');
                break;
                case 'alltime':
                    $start_date = '2000-01-01 00:00:00'; // random early record
                    $end_date = date('Y-m-d 23:59:59');
                    break;
            case 'custom':
                // Dates already set
                break;
        }


// Prepare SQL statement
$sql = "SELECT * FROM orders  WHERE order_date BETWEEN ? AND ? AND status != 'Cancelled'";
$stmt = mysqli_prepare($conn, $sql);

if ($stmt) {
    // Bind parameters
    mysqli_stmt_bind_param($stmt, "ss", $start_date, $end_date);

    // Execute statement
    mysqli_stmt_execute($stmt);

    // Get results
    $res = mysqli_stmt_get_result($stmt);

    // Process fetched data
    while ($row = mysqli_fetch_assoc($res)) {
        $orders[] = $row;
        $total_profit += $row['total']; // Calculate profit
    }

    // Close statement
    mysqli_stmt_close($stmt);

    // Handle CSV export if requested
    if (isset($_POST['export_csv'])) {
        generateCSV($orders, $total_profit);
    }
} else {
    // Handle statement preparation error
    echo "Error: " . mysqli_error($conn);
}
    }
}

// Function to generate CSV report
function generateCSV($orders, $total_profit) {
    // Set headers for CSV download
    $filename = "cashflow_report.csv";
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment;filename="'.$filename.'"');

    // Open output stream
    $output = fopen('php://output', 'w');

    // Write header row
    fputcsv($output, ['Order ID', 'Customer Name', 'Total', 'Order Date']);

    // Write data rows
    foreach ($orders as $order) {
        fputcsv($output, [$order['id'], $order['customer_name'], $order['total'], $order['order_date']]);
    }

    // Write total profit row
    fputcsv($output, []); // Empty row
    fputcsv($output, ['Total Profit', number_format($total_profit, 2)]);

    // Close output stream
    fclose($output);

    // Exit script
    exit();
}

?>

<div class="main-content">
    <div class="wrapper">
        <h1>Cash Flow Report</h1>
        <br /><br />

        <!-- Form for selecting timeframe -->
        <form method="POST" action="">
            <label for="timeframe">Select Timeframe:</label>
            <select name="timeframe" id="timeframe" required>
    <option value="today">Today</option>
    <option value="week">Last Week</option>
    <option value="month">Last Month</option>
    <option value="6months">Last 6 Months</option>
    <option value="year">Last Year</option>
    <option value="custom">Custom Date Range</option>
    <option value="alltime">All Time</option>
            </select>
            <br><br>

            <div id="custom-date-range" style="display: none;">
                <label for="start_date">Start Date:</label>
                <input type="date" name="start_date" id="start_date">
                <br><br>
                <label for="end_date">End Date:</label>
                <input type="date" name="end_date" id="end_date">
                <br><br>
            </div>

            <input type="submit" name="generate_report" value="Generate Report" class="btn-primary">
            <br><br>
        </form>

        <!-- Separate form for CSV download -->
        <?php if (!empty($orders)): ?>
            <form method="POST" action="">
                <input type="hidden" name="timeframe" value="<?php echo $timeframe; ?>">
                <input type="hidden" name="start_date" value="<?php echo $start_date; ?>">
                <input type="hidden" name="end_date" value="<?php echo $end_date; ?>">
                <input type="submit" name="export_csv" value="Download CSV" class="btn-secondary">
                <br><br>
                <button type="button" id="export_pdf" class="btn-secondary">Download PDF</button>
            </form>
        <?php endif; ?>

        <!-- Add JavaScript to handle custom date range display -->
        <script>
            document.getElementById('timeframe').addEventListener('change', function() {
                if (this.value === 'custom') {
                    document.getElementById('custom-date-range').style.display = 'block';
                } else {
                    document.getElementById('custom-date-range').style.display = 'none';
                }
            });
        </script>

        <?php if (!empty($orders)): ?>
            <h2><?php echo $start_date; ?> to <?php echo $end_date; ?></h2>
            <table class="tbl-full" id="cashflow-table">
                <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Total</th>
                    <th>Order Date</th>
                </tr>
                <?php foreach ($orders as $order): ?>
                    <tr>
                        <td><?php echo $order['id']; ?></td>
                        <td><?php echo $order['customer_name']; ?></td>
                        <td>RM<?php echo $order['total']; ?></td>
                        <td><?php echo $order['order_date']; ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="3" style="text-align: right;"><strong>Total Profit:</strong></td>
                    <td><strong>RM<?php echo number_format($total_profit, 2); ?></strong></td>
                </tr>
            </table>
        <?php else: ?>
            <p>No orders found for the selected timeframe.</p>
        <?php endif; ?>
    </div>
</div>

<?php include('admin-sections/footer.php'); ?>

<!-- jsPDF Library -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.4.0/jspdf.umd.min.js"></script>
<script>
    document.getElementById('export_pdf').addEventListener('click', function() {
        const { jsPDF } = window.jspdf;
        const doc = new jsPDF();

        let content = `
            Cash Flow Report
            <?php echo $start_date; ?> to <?php echo $end_date; ?>

        `;

        const rows = document.querySelectorAll('#cashflow-table tr');
        rows.forEach((row, index) => {
            const cells = row.querySelectorAll('td, th');
            cells.forEach(cell => {
                content += (index > 0 ? '    ' : '') + cell.innerText + (index === 0 ? ', ' : '    ');
            });
            content += '\n';
        });

        content += `Total Profit: RM<?php echo number_format($total_profit, 2); ?>`;

        doc.text(content, 10, 10);
        doc.save('cashflow_report.pdf');
    });
</script>
