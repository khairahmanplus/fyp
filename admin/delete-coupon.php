<?php
include('admin-sections/header.php');

if (isset($_GET['couponCode'])) {
    $couponCode = $_GET['couponCode'];

    // SQL Query to delete the coupon
    $sql = "DELETE FROM coupon WHERE couponCode='$couponCode'";
    $res = mysqli_query($conn, $sql);

    if ($res) {
        $_SESSION['delete'] = "<div class='success'>Coupon Deleted Successfully.</div>";
        header("location:" . SITEURL . 'admin/manage-coupon.php');
        exit;
    } else {
        $_SESSION['delete'] = "<div class='error'>Failed to Delete Coupon.</div>";
        header("location:" . SITEURL . 'admin/manage-coupon.php');
        exit;
    }
} else {
    header('location:' . SITEURL . 'admin/manage-coupon.php');
    exit;
}
?>

<?php include('admin-sections/footer.php'); ?>
