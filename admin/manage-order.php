<?php include('admin-sections/header.php'); ?>

<?php 
    // Check whether the Update Button is Clicked or Not
    if(isset($_POST['submit']))
    {
        // Get All the Values from Form
        $id = $_POST['id'];
        $status = $_POST['status'];

        // Update the Values
        $sql2 = "UPDATE orders SET status = '$status' WHERE id=$id";

        // Execute the Query
        $res2 = mysqli_query($conn, $sql2);

        // Check whether update or not
        // And Redirect to Manage Order with Message
        if($res2 == true)
        {
            // Updated
            $_SESSION['update'] = "<div class='success'>Order #$id status updated successfully to $status.</div>";
        }
        else
        {
            // Failed to Update
            $_SESSION['update'] = "<div class='error'>Failed to update order.</div>";
        }
    }

    // Initialize variables for date range, order status, and exclude canceled orders
    $start_date = null;
    $end_date = null;
    $delivery_start_date = null;
    $delivery_end_date = null;
    $selected_status = 'all';
    $timeframe = 'alltime';
    $delivery_timeframe = 'alltime'; // Initialize delivery timeframe
    $exclude_cancelled = isset($_POST['exclude_cancelled']) ? $_POST['exclude_cancelled'] : '';

    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['filter_orders'])) {
        // Handle form submission for filtering orders
        $timeframe = $_POST['timeframe'];
        $selected_status = $_POST['order_status'];
        $exclude_cancelled = isset($_POST['exclude_cancelled']) ? $_POST['exclude_cancelled'] : '';

        $start_date = isset($_POST['start_date']) ? $_POST['start_date'] : null;
        $end_date = isset($_POST['end_date']) ? $_POST['end_date'] : null;

        // Delivery date range based on selected timeframe
        $delivery_timeframe = $_POST['delivery_timeframe'];
        switch($delivery_timeframe) {
            case 'any':
                $start_date = '1000-01-01';
                $end_date = '9999-01-01';
                break;
            case 'tomorrow':
                $delivery_start_date = date('Y-m-d');
                $delivery_end_date = date('Y-m-d', strtotime('+1 day'));
                break;
            case 'week':
                $delivery_start_date = date('Y-m-d');
                $delivery_end_date = date('Y-m-d', strtotime('+1 week'));
                break;
            case 'month':
                $delivery_start_date = date('Y-m-d');
                $delivery_end_date = date('Y-m-d', strtotime('+1 month'));
                break;
            case '6months':
                $delivery_start_date = date('Y-m-d');
                $delivery_end_date = date('Y-m-d', strtotime('+6 months'));
                break;
            case 'year':
                $delivery_start_date = date('Y-m-d');
                $delivery_end_date = date('Y-m-d', strtotime('+1 year'));
                break;
            case 'custom':
                $delivery_start_date = isset($_POST['delivery_start_date']) ? $_POST['delivery_start_date'] : null;
                $delivery_end_date = isset($_POST['delivery_end_date']) ? $_POST['delivery_end_date'] : null;
                break;
            case 'future':
                $delivery_start_date = date('Y-m-d');
                $delivery_end_date = '9999-01-01';
                break;
        }
        
        // Determine date range based on selected timeframe
        switch($timeframe) {
            case 'today':
                $start_date = date('Y-m-d');
                $end_date = date('Y-m-d');
                break;
            case 'week':
                $start_date = date('Y-m-d', strtotime('-1 week'));
                $end_date = date('Y-m-d');
                break;
            case 'month':
                $start_date = date('Y-m-d', strtotime('-1 month'));
                $end_date = date('Y-m-d');
                break;
            case '6months':
                $start_date = date('Y-m-d', strtotime('-6 months'));
                $end_date = date('Y-m-d');
                break;
            case 'year':
                $start_date = date('Y-m-d', strtotime('-1 year'));
                $end_date = date('Y-m-d');
                break;
            case 'custom':
                $start_date = isset($_POST['start_date']) ? $_POST['start_date'] : null;
                $end_date = isset($_POST['end_date']) ? $_POST['end_date'] : null;
                break;
            case 'alltime':
                $start_date = '2000-01-01';
                $end_date = date('Y-m-d');
                break;
        }
    }
?>

<div class="main-content">
    <div class="wrapper">
        <h1>Manage Food Order</h1>

        <br /><br /><br />

        <?php 
            if(isset($_SESSION['update']))
            {
                echo $_SESSION['update'];
                unset($_SESSION['update']);
            }
        ?>
        <br><br>

        <!-- Form for selecting timeframe and order status -->
        <form method="POST" action="">
            <label for="timeframe">Select Order Date Timeframe:</label>
            <select name="timeframe" id="timeframe" required>
                <option value="today" <?php if ($timeframe == 'today') echo 'selected'; ?>>Today</option>
                <option value="week" <?php if ($timeframe == 'week') echo 'selected'; ?>>Last Week</option>
                <option value="month" <?php if ($timeframe == 'month') echo 'selected'; ?>>Last Month</option>
                <option value="6months" <?php if ($timeframe == '6months') echo 'selected'; ?>>Last 6 Months</option>
                <option value="year" <?php if ($timeframe == 'year') echo 'selected'; ?>>Last Year</option>
                <option value="custom" <?php if ($timeframe == 'custom') echo 'selected'; ?>>Custom Date Range</option>
                <option value="alltime" <?php if ($timeframe == 'alltime') echo 'selected'; ?>>All Time</option>
            </select>
            <br><br>

            <div id="custom-date-range" style="display: <?php echo $timeframe == 'custom' ? 'block' : 'none'; ?>;">
                <label for="start_date">Start Date:</label>
                <input type="date" name="start_date" id="start_date" value="<?php echo isset($start_date) ? $start_date : ''; ?>">
                <br><br>
                <label for="end_date">End Date:</label>
                <input type="date" name="end_date" id="end_date" value="<?php echo isset($end_date) ? $end_date : ''; ?>">
                <br><br>
            </div>

            <label for="delivery_timeframe">Select Delivery Date Timeframe:</label>
            <select name="delivery_timeframe" id="delivery_timeframe" required>
                <option value="any" <?php if ($delivery_timeframe == 'any') echo 'selected'; ?>>Any</option>
                <option value="tomorrow" <?php if ($delivery_timeframe == 'tomorrow') echo 'selected'; ?>>Tomorrow</option>
                <option value="week" <?php if ($delivery_timeframe == 'week') echo 'selected'; ?>>Next Week</option>
                <option value="month" <?php if ($delivery_timeframe == 'month') echo 'selected'; ?>>Next Month</option>
                <option value="6months" <?php if ($delivery_timeframe == '6months') echo 'selected'; ?>>Next 6 Months</option>
                <option value="year" <?php if ($delivery_timeframe == 'year') echo 'selected'; ?>>Next Year</option>
                <option value="custom" <?php if ($delivery_timeframe == 'custom') echo 'selected'; ?>>Custom Date Range</option>
                <option value="future" <?php if ($delivery_timeframe == 'future') echo 'selected'; ?>>Future</option>
            </select>
            <br><br>

            <div id="custom-delivery-date-range" style="display: <?php echo $delivery_timeframe == 'custom' ? 'block' : 'none'; ?>;">
                <label for="delivery_start_date">Start Date:</label>
                <input type="date" name="delivery_start_date" id="delivery_start_date" value="<?php echo isset($delivery_start_date) ? $delivery_start_date : ''; ?>">
                <br><br>
                <label for="delivery_end_date">End Date:</label>
                <input type="date" name="delivery_end_date" id="delivery_end_date" value="<?php echo isset($delivery_end_date) ? $delivery_end_date : ''; ?>">
                <br><br>
            </div>

            <label for="order_status">Select Order Status:</label>
            <select name="order_status" id="order_status" required>
                <option value="all" <?php if ($selected_status == 'all') echo 'selected'; ?>>All</option>
                <option value="Ordered" <?php if ($selected_status == 'Ordered') echo 'selected'; ?>>Ordered</option>
                <option value="Processing" <?php if ($selected_status == 'Processing') echo 'selected'; ?>>Processing</option>
                <option value="Ready" <?php if ($selected_status == 'Ready') echo 'selected'; ?>>Ready</option>
                <option value="Delivered" <?php if ($selected_status == 'Delivered') echo 'selected'; ?>>Delivered</option>
                <option value="notcancelled" <?php if ($selected_status == 'notcancelled') echo 'selected'; ?>>All Except Cancelled</option>
            </select>
            <br><br>


            <input type="submit" name="filter_orders" value="Filter Orders" class="btn-primary">
            <br><br>
        </form>

        <table class="tbl-full">
            <tr>
                <th width="10%">Order Date</th>
                <th width="10%">Delivery Date</th>
                <th width="35%">Order Details</th>
                <th width="25%">Customer Details</th>
                <th>Status</th>
                <th>Print</th>
            </tr>

            <?php 
                // Get all the orders from the database based on the selected timeframe and status
                $sql = "SELECT * FROM orders";
                $conditions = array();
                
                if ($start_date && $end_date) {
                    $conditions[] = "order_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'";
                }
                
                if ($delivery_start_date && $delivery_end_date) {
                    $conditions[] = "delivery_date BETWEEN '$delivery_start_date' AND '$delivery_end_date'";
                }

                if ($selected_status && $selected_status != 'all') {
                    if ($selected_status == 'notcancelled') {
                        $conditions[] = "status != 'Cancelled'";
                    } else {
                        $conditions[] = "status = '$selected_status'";
                    }
                }
                
                if ($exclude_cancelled) {
                    $conditions[] = "status != 'Cancelled'";
                }
                
                if (count($conditions) > 0) {
                    $sql .= " WHERE " . implode(" AND ", $conditions);
                }
                
                $sql .= " ORDER BY id DESC"; // Display the Latest Order at First

                // Execute Query
                $res = mysqli_query($conn, $sql);
                // Count the Rows
                $count = mysqli_num_rows($res);

                $sn = 1; // Create a Serial Number and set its initial value as 1

                if($count > 0)
                {
                    // Order Available
                    while($row = mysqli_fetch_assoc($res))
                    {
                        // Get all the order details
                        $id = $row['id'];
                        $food = $row['food'];
                        $total = $row['total'];
                        $payment = $row['payment'];
                        $order_date = $row['order_date'];
                        $delivery_date = $row['delivery_date']; // Added delivery date
                        $remark = $row['remark'];
                        $status = $row['status'];
                        $customer_name = $row['customer_name'];
                        $customer_contact = $row['customer_contact'];
                        $customer_email = $row['customer_email'];
                        $customer_address = $row['customer_address'];
                        
                        ?>

                        <tr>
                            <td><?php echo $order_date; ?></td>
                            <td><?php echo $delivery_date; ?></td> <!-- Display delivery date -->
                            <td>
                                Order ID: <?php echo $id; ?><br>
                                <?php echo $food; ?><br>
                                Method: <?php echo $payment; ?><br>
                                Remark: <?php echo ($remark == "") ? "None" : $remark; ?>
                            </td>
                            <td>
                                Name: <?php echo $customer_name; ?><br>
                                Tel. No: <?php echo $customer_contact; ?><br>
                                Email: <?php echo $customer_email; ?><br>
                                Address: <?php echo $customer_address; ?>
                            </td>
                            <td class="text-center" style="
                                <?php 
                                    // Ordered, Processing, Ready, Delivered, Cancelled
                                    if($status == "Ordered")
                                    {
                                        echo "background-color: Silver";
                                    }
                                    elseif($status == "Processing")
                                    {
                                        echo "background-color: SlateBlue";
                                    }
                                    elseif($status == "Ready")
                                    {
                                        echo "background-color: LightSeaGreen";
                                    }
                                    elseif($status == "Delivered")
                                    {
                                        echo "background-color: green;";
                                    }
                                    elseif($status == "Cancelled")
                                    {
                                        echo "background-color: red;";
                                    }
                                ?>
                            ">
                                <form action="" method="POST" name="form<?php echo $id; ?>">
                                    <select name="status">
                                        <option <?php if($status == "Ordered"){echo "selected";} ?> value="Ordered" style="color: Silver;">Ordered</option>
                                        <option <?php if($status == "Processing"){echo "selected";} ?> value="Processing" style="color: SlateBlue;">Processing</option>
                                        <option <?php if($status == "Ready"){echo "selected";} ?> value="Ready" style="color: LightSeaGreen;">Ready</option>
                                        <option <?php if($status == "Delivered"){echo "selected";} ?> value="Delivered" style="color: green;">Delivered</option>
                                        <option <?php if($status == "Cancelled"){echo "selected";} ?> value="Cancelled" style="color: red;">Cancelled</option>
                                    </select>

                                    <br><br>
                                    <input type="hidden" name="id" value="<?php echo $id; ?>">
                                    <input type="submit" name="submit" value="Update Status" class="btn-secondary">
                                </form>
                            </td>
                            <td>
                                <!-- Add a button to trigger the JavaScript function for printing receipt -->
                                <center><button onclick="printReceipt('<?php echo $id; ?>', '<?php echo $customer_name; ?>', '<?php echo $order_date; ?>', '<?php echo $payment; ?>', '<?php echo $food; ?>')">Print Receipt</button></center>
                            </td>
                        </tr>

                        <?php
                    }
                }
                else
                {
                    // Order not Available
                    echo "<tr><td colspan='12' class='error'>Orders not Available</td></tr>";
                }
            ?>
        </table>
    </div>
</div>

<script>
    // JavaScript function to show/hide custom date range inputs
    document.getElementById('timeframe').addEventListener('change', function() {
        var customDateRange = document.getElementById('custom-date-range');
        if (this.value === 'custom') {
            customDateRange.style.display = 'block';
        } else {
            customDateRange.style.display = 'none';
        }
    });

    document.getElementById('delivery_timeframe').addEventListener('change', function() {
        var customDeliveryDateRange = document.getElementById('custom-delivery-date-range');
        if (this.value === 'custom') {
            customDeliveryDateRange.style.display = 'block';
        } else {
            customDeliveryDateRange.style.display = 'none';
        }
    });

    // JavaScript function to print receipts
    function printReceipt(orderId, customerName, orderDate, paymentMethod, food) {
        // Create the receipt content
        var receiptContent = `
            <span><b><center>Auntie Jue Home Bakes</center></b></span>
            <table style='width:100%;border: 1px solid black'>
                <tr>
                    <td>Bill to: ${customerName}</td>
                    <td style='text-align:right'>Receipt No.: ${orderId}</td>
                </tr>
                <tr>
                    <td>Payment method: ${paymentMethod}</td>
                    <td style='text-align:right'>Order date and time: ${orderDate}</td>
                </tr>
            </table>
            <br>
            <div style='text-align:right'>Item (Price x Quantity = Total)<br>${food}</div>
            <div style='text-align:center'><br>Thank you for your business!</div>
        `;

        // Open a new window with the receipt content
        var receiptWindow = window.open("", "PRINT", "height=600,width=600");
        receiptWindow.document.write(receiptContent);
        receiptWindow.document.close();
        receiptWindow.focus();
        receiptWindow.print();
        return true;
    }
</script>

<?php include('admin-sections/footer.php'); ?>
