<?php
include('admin-sections/header.php');
// Check whether the couponCode is set or not
if (isset($_GET['couponCode'])) {
    $couponCode = $_GET['couponCode'];

    // SQL Query to get the details
    $sql = "SELECT * FROM coupon WHERE couponCode='$couponCode'";
    $res = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($res);

    $discountValue = $row['discountValue'];
    $isActive = $row['isActive'];
} else {
    header('location:' . SITEURL . 'admin/manage-coupon.php');
}
?>

<div class="main-content">
    <div class="wrapper">
        <h1>Update Coupon</h1>

        <br /><br />

        <form action="" method="POST">
            <table class="tbl-30">
                <tr>
                    <td>Coupon Code: </td>
                    <td><input type="text" name="couponCode" value="<?php echo $couponCode; ?>" ></td>
                </tr>

                <tr>
                    <td>Discount Value (decimal): </td>
                    <td><input type="number" name="discountValue" value="<?php echo $discountValue; ?>" step="0.01" min="0" max="100"></td>
                </tr>

                <tr>
                    <td>Is Active: </td>
                    <td>
                        <input <?php if ($isActive == 1) { echo "checked"; } ?> type="radio" name="isActive" value="1"> Yes
                        <input <?php if ($isActive == 0) { echo "checked"; } ?> type="radio" name="isActive" value="0"> No
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <input type="submit" name="submit" value="Update Coupon" class="btn-primary">
                    </td>
                </tr>
            </table>
        </form>

        <?php
        if (isset($_POST['submit'])) {
            $discountValue = mysqli_real_escape_string($conn, $_POST['discountValue']);
            $isActive = isset($_POST['isActive']) ? $_POST['isActive'] : 0;

            // SQL Query to update the coupon
            $sql2 = "UPDATE coupon SET discountValue='$discountValue', isActive='$isActive' WHERE couponCode='$couponCode'";

            // Execute the Query
            $res2 = mysqli_query($conn, $sql2);

            // Check whether the query executed successfully or not
            if ($res2 == TRUE) {
                $_SESSION['update'] = "<div class='success'>Coupon Updated Successfully.</div>";
                header("location:" . SITEURL . 'admin/manage-coupon.php');
            } else {
                $_SESSION['update'] = "<div class='error'>Failed to Update Coupon.</div>";
                header("location:" . SITEURL . 'admin/manage-coupon.php');
                }
                }
                ?>
                
                </div>
                </div>
                <?php include('admin-sections/footer.php'); ?>
