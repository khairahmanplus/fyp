<?php include('admin-sections/constants.php');
if(isset($_POST['submit'])){

$filter_username = filter_var($_POST['username'], FILTER_SANITIZE_STRING);
$username = mysqli_real_escape_string($conn, $filter_username);
$filter_pass = filter_var($_POST['password'], FILTER_SANITIZE_STRING);
$pass = mysqli_real_escape_string($conn, md5($filter_pass));

$select_users = mysqli_query($conn, "SELECT * FROM `admin` WHERE username = '$username' AND password = '$pass'") or die('query failed');


if(mysqli_num_rows($select_users) > 0){
   
   $row = mysqli_fetch_assoc($select_users);

   if($row['username'] == $username){

      $_SESSION['admin'] = $row['username'];
      header('location:index.php');

   }else{
      $message[] = 'no user found!';
   }

}else{
   $message[] = 'incorrect username or password!';
}

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>AJHB Admin Login</title>

   <!-- font awesome cdn link  -->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
   <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

   <!-- custom css file link  -->
   <link rel="stylesheet" href="../assets/login.css">

</head>
<body>
<?php
if(isset($message)){
   foreach($message as $message){
      echo '
      <div class="message">
         <span>'.$message.'</span>
         <i class="fas fa-times" onclick="this.parentElement.remove();"></i>
      </div>
      ';
   }
}if(isset($_SESSION['message'])){
       echo '
       <div class="message">
          <span>'.$_SESSION['message'].'</span>
          <i class="fas fa-times" onclick="this.parentElement.remove();"></i>
       </div>
       ';
       unset($_SESSION['message']);
 }
 
?>
   
<section class="form-container">
   <form action="" method="post" name="login">
      <h3>admin login</h3>
      <input type="text" name="username" class="box" placeholder="enter your username" required>
      <input type="password" name="password" class="box" placeholder="enter your password" required>
      <input type="submit" class="btn" name="submit" value="login now">
   </form>
</section>

</body>
</html>
