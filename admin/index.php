
<?php include('admin-sections/header.php'); 
$date = date("Y-m-d");
?>

        <!-- Main Content Section Starts -->
        <div class="main-content">
            <div class="wrapper">
                <center><h1>Auntie Jue Home Bakes Dashboard</h1></center>
                <br><br>
                <br><br>


                <div class="col-4 text-center">

                    <?php 
                        //Sql Query 
                        $sql2 = "SELECT * FROM `food`";// 
                        //Execute Query
                        $res2 = mysqli_query($conn, $sql2);
                        //Count Rows
                        $count2 = mysqli_num_rows($res2);
                    ?>

                    <h1><?php echo $count2; ?></h1>
                    <br />
                    Foods
                </div>

                <div class="col-4 text-center">

                    <?php 
                        //Sql Query 
                        $sql2 = "SELECT * FROM `category`";//
                        //Execute Query
                        $res2 = mysqli_query($conn, $sql2);
                        //Count Rows
                        $count2 = mysqli_num_rows($res2);
                    ?>

                    <h1><?php echo $count2; ?></h1>
                    <br />
                    Categories
                </div>

                <div class="col-4 text-center">
                    
                    <?php 
                        //Sql Query 
                        $sql3 = "SELECT * FROM `orders` WHERE order_date LIKE '$date%'";
                        //Execute Query
                        $res3 = mysqli_query($conn, $sql3);
                        //Count Rows
                        $count3 = mysqli_num_rows($res3);
                    ?>

                    <h1><?php echo $count3; ?></h1>
                    <br />
                    Today's Orders
                </div>

                
                <div class="col-4 text-center">
                    
                    <?php 
                        //Sql Query 
                        $sql6 = "SELECT * FROM `orders` WHERE status = 'Cancelled'";
                        //Execute Query
                        $res6 = mysqli_query($conn, $sql6);
                        //Count Rows
                        $count6 = mysqli_num_rows($res6);
                    ?>

                    <h1><?php echo $count6; ?></h1>
                    <br />
                    Cancelled Orders
                </div>

                <div class="col-4 text-center">
                    
                    <?php 
                        //Sql Query 
                        $sql6 = "SELECT * FROM `orders` WHERE status = 'Ordered'";
                        //Execute Query
                        $res6 = mysqli_query($conn, $sql6);
                        //Count Rows
                        $count6 = mysqli_num_rows($res6);
                    ?>

                    <h1><?php echo $count6; ?></h1>
                    <br />
                    Pending Orders
                </div>



                <div class="clearfix"></div>

                <div class="col-4 text-center">
                    
                    <?php 
                        //Sql Query 
                        $sql7 = "SELECT * FROM `orders` WHERE status = 'Processing'";
                        //Execute Query
                        $res7 = mysqli_query($conn, $sql7);
                        //Count Rows
                        $count7 = mysqli_num_rows($res7);
                    ?>

                    <h1><?php echo $count7; ?></h1>
                    <br />
                    Processing Orders
                </div>

                <div class="col-4 text-center">
                    
                    <?php 
                        //Sql Query 
                        $sql7 = "SELECT * FROM `orders` WHERE status = 'Ready'";
                        //Execute Query
                        $res7 = mysqli_query($conn, $sql7);
                        //Count Rows
                        $count7 = mysqli_num_rows($res7);
                    ?>

                    <h1><?php echo $count7; ?></h1>
                    <br />
                    Ready Orders
                </div>
                <div class="col-4 text-center">
                    
                    <?php 
                        //Sql Query 
                        $sql3 = "SELECT * FROM `orders` WHERE status = 'Delivered'";
                        //Execute Query
                        $res3 = mysqli_query($conn, $sql3);
                        //Count Rows
                        $count3 = mysqli_num_rows($res3);
                    ?>

                    <h1><?php echo $count3; ?></h1>
                    <br />
                    Delivered Orders
                </div>


                <div class="col-4 text-center">

                <?php 
                        $total_revenue = 0;
                        //Creat SQL Query to Get daily Revenue Generated
                        //Aggregate Function in SQL
                        $sql4 = "SELECT SUM(total) AS Total FROM `orders` WHERE status != 'Cancelled' and order_date LIKE '$date%'";

                        //Execute the Query
                        $res4 = mysqli_query($conn, $sql4);

                        //Get the VAlue
                        $row4 = mysqli_fetch_assoc($res4);
                        
                        //GEt the Total REvenue
                        $total_revenue = $total_revenue + $row4['Total'];

                    ?>

                    <h1>RM<?php echo number_format((float)($total_revenue), 2, '.', ''); ?></h1>     
                    <br />              
                    Today's Cashflow (<?php echo $date; ?>)
                    <br>
                </div>

                <div class="col-4 text-center">
                    
                    <?php 
                        $total_revenue = 0;
                        //Creat SQL Query to Get Total Revenue Generated
                        //Aggregate Function in SQL
                        $sql4 = "SELECT SUM(total) AS Total FROM `orders` WHERE status != 'Cancelled'";

                        //Execute the Query
                        $res4 = mysqli_query($conn, $sql4);

                        //Get the VAlue
                        $row4 = mysqli_fetch_assoc($res4);
                        
                        //GEt the Total REvenue
                        $total_revenue = $total_revenue + $row4['Total'];

                    ?>

                    <h1>RM<?php echo number_format((float)($total_revenue), 2, '.', ''); ?></h1>
                    <br />
                    Total Revenue
                </div>

                <div class="clearfix"></div>


            </div>
<br><br>
        </div>
        <!-- Main Content Setion Ends -->
<?php include('admin-sections/footer.php') ?>