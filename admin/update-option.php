<?php include('admin-sections/header.php'); ?>

<div class="main-content">
    <div class="wrapper">
        <h1>Update Option</h1>

        <br><br>

        <?php 
        
            //Check whether the id is set or not
            if(isset($_GET['id']))
            {
                //Get the ID and all other details
                $id = $_GET['id'];

                //Create SQL Query to Get all other details
                $sql = "SELECT * FROM `option` WHERE id=$id";

                //Execute the Query
                $res = mysqli_query($conn, $sql);

                //Count the Rows to check whether the id is valid or not
                $count = mysqli_num_rows($res);

                if($count==1)
                {
                    //Get all the data
                    $row = mysqli_fetch_assoc($res);
                    $name = $row['optionName'];
                    $description = $row['optionDesc'];
                    $current_image = $row['optionImg'];
                }
                else
                {
                    //Redirect to Manage Option with Session Message
                    $_SESSION['no-option-found'] = "<div class='error'>Option not Found.</div>";
                    header('location:'.SITEURL.'admin/manage-option.php');
                }
            }
            else
            {
                //Redirect to Manage Option
                header('location:'.SITEURL.'admin/manage-option.php');
            }
        
        ?>

        <form action="" method="POST" enctype="multipart/form-data">

            <table class="tbl-30">
                <tr>
                    <td>Name: </td>
                    <td>
                        <input type="text" name="name" value="<?php echo $name; ?>">
                    </td>
                </tr>

                <tr>
                    <td>Description: </td>
                    <td>
                        <input type="text" name="description" value="<?php echo $description; ?>">
                    </td>
                </tr>

                <tr>
                    <td>Current Image: </td>
                    <td>
                        <?php 
                            if($current_image != "")
                            {
                                //Display the Image
                                ?>
                                <img src="<?php echo SITEURL; ?>images/options/<?php echo $current_image; ?>" width="150px">
                                <?php
                            }
                            else
                            {
                                //Display Message
                                echo "<div class='error'>Image Not Added.</div>";
                            }
                        ?>
                    </td>
                </tr>

                <tr>
                    <td>Select New Image: </td>
                    <td>
                        <input type="file" name="image">
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <input type="hidden" name="current_image" value="<?php echo $current_image; ?>">
                        <input type="hidden" name="id" value="<?php echo $id; ?>">
                        <input type="submit" name="submit" value="Update Option" class="btn-secondary">
                    </td>
                </tr>

            </table>

        </form>

        <?php 
        
            if(isset($_POST['submit']))
            {
                //1. Get all the details from the form
                $id = $_POST['id'];
                $name = $_POST['name'];
                $description = $_POST['description'];
                $current_image = $_POST['current_image'];

                //2. Updating New Image if selected
                //Check whether the image is selected or not
                if(isset($_FILES['image']['name']))
                {
                    //Get the image details
                    $image_name = $_FILES['image']['name'];

                    //Check whether the image is available or not
                    if($image_name != "")
                    {
                        //Image Available
                        //A. Uploading New Image

                        //Auto Rename our Image
                        $ext = end(explode('.', $image_name)); //Gets the extension of the image

                        $image_name = "Option_".rand(000, 999).'.'.$ext; //This will be renamed image

                        $source_path = $_FILES['image']['tmp_name']; //Source Path
                        $destination_path = "../images/options/".$image_name; //Destination Path

                        //Finally Upload the Image
                        $upload = move_uploaded_file($source_path, $destination_path);

                        //Check whether the image is uploaded or not
                        if($upload==false)
                        {
                            //Set Message
                            $_SESSION['upload'] = "<div class='error'>Failed to Upload Image. </div>";
                            //Redirect to Manage Option Page
                            header('location:'.SITEURL.'admin/manage-option.php');
                            //Stop the Process
                            die();
                        }

                        //B. Remove Current Image if available
                        if($current_image != "")
                        {
                            $remove_path = "../images/options/".$current_image;

                            $remove = unlink($remove_path);

                            //Check whether the image is removed or not
                            //If failed to remove then display message and stop the process
                            if($remove==false)
                            {
                                //Failed to remove image
                                $_SESSION['failed-remove'] = "<div class='error'>Failed to Remove Current Image.</div>";
                                header('location:'.SITEURL.'admin/manage-option.php');
                                die();
                            }
                        }
                        

                    }
                    else
                    {
                        $image_name = $current_image;
                    }
                }
                else
                {
                    $image_name = $current_image;
                }

                //3. Update the Database
                $sql2 = "UPDATE `option` SET 
                    optionName = '$name',
                    optionDesc = '$description',
                    optionImg = '$image_name'
                    WHERE id=$id
                ";

                //Execute the Query
                $res2 = mysqli_query($conn, $sql2);

                //4. Redirect to Manage Option with Message
                //Check whether executed or not
                if($res2==true)
                {
                    //Option Updated
                    $_SESSION['update'] = "<div class='success'>Option Updated Successfully.</div>";
                    header('location:'.SITEURL.'admin/manage-option.php');
                }
                else
                {
                    //Failed to Update Option
                    $_SESSION['update'] = "<div class='error'>Failed to Update Option.</div>";
                    header('location:'.SITEURL.'admin/manage-option.php');
                }

            }
        
        ?>

    </div>
</div>

<?php include('admin-sections/footer.php'); ?>
