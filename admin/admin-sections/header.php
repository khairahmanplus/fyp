<?php 

    include('../config/constants.php'); 
    include('login-check.php');

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>AJHB Admin</title>
	<link rel="stylesheet" type="text/css" href="../assets/style.css">

	<link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">
	<!--
    - favicon
  	
  	<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">-->

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">

</head>
<body>
	<!----header section--->
	<header style="background-color: #f6f3f0">
		<a href="index.php" class="logo">AJHB Admin</a>

		<ul class="navlist">

                    <li>
                        <a href="index.php">Dashboard</a>
                    </li>
                    <li>
                        <a href="manage-order.php">Orders</a>
                    </li>
                    <li>
                        <a href="cashflow.php">Cash Flow</a>
                    </li>
                    <li>
                        <a href="manage-food.php">Foods</a>
                    </li>
                    <li>
                        <a href="manage-category.php">Categories</a>
                    </li>
                    <li>
                        <a href="manage-option.php">Options</a>
                    </li>
                    
                    <li>
                        <a href="manage-coupon.php">Coupons</a>
                    </li>
                    <li>
                        <a href="feedback.php">Feedback</a>
                    </li>
                    <li>
                    <a href="logout.php">Logout</a>
                    </li>
                </ul>
            </div>

            <div class="clearfix"></div>
        </div>
		<div class="header-icons">
            <a href="manage-admin.php"><i class='bx bx-user'></i></a>
			<div class="bx bx-menu" id="menu-icon"></div>
		</div>
	</header>
<br>
<br>
