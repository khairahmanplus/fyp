<?php 
include('admin-sections/header.php'); 

// Fetch all feedbacks
$feedback_sql = "SELECT f.feedbackMessage, f.timeSubmitted, c.username 
                 FROM feedback f 
                 JOIN customers c ON f.custId = c.id 
                 ORDER BY f.timeSubmitted DESC";
$feedback_res = mysqli_query($conn, $feedback_sql);

?>

<div class="main-content">
    <div class="wrapper">
        <h1>Manage Feedback</h1>

        <br /><br />

        <!-- Feedback Table -->
        <table class="tbl-full">
            <tr>
                <th>#</th>
                <th>Customer Username</th>
                <th>Feedback Message</th>
                <th>Time Submitted</th>
                <th>Actions</th>
            </tr>

            <?php
            $count = mysqli_num_rows($feedback_res);
            $sn = 1;

            if ($count > 0) {
                while ($row = mysqli_fetch_assoc($feedback_res)) {
                    $feedbackMessage = $row['feedbackMessage'];
                    $timeSubmitted = $row['timeSubmitted'];
                    $username = $row['username'];
                    ?>
                    <tr>
                        <td><?php echo $sn++; ?>. </td>
                        <td><?php echo $username; ?></td>
                        <td><?php echo substr($feedbackMessage, 0, 50); ?>...</td>
                        <td><?php echo $timeSubmitted; ?></td>
                        <td>
                            <button class="btn-primary" onclick="toggleDetails('details-<?php echo $sn; ?>')">View Details</button>
                        </td>
                    </tr>
                    <tr id="details-<?php echo $sn; ?>" style="display: none;">
                        <td colspan="5">
                            <div class="feedback-details">
                                <p><?php echo $feedbackMessage; ?></p>
                            </div>
                        </td>
                    </tr>
            <?php
                }
            } else {
                echo "<tr> <td colspan='5' class='error'> No Feedback Available. </td> </tr>";
            }
            ?>
        </table>
    </div>
</div>

<script>
function toggleDetails(id) {
    var element = document.getElementById(id);
    if (element.style.display === "none") {
        element.style.display = "table-row";
    } else {
        element.style.display = "none";
    }
}
</script>

<?php include('admin-sections/footer.php'); ?>
