<?php include('admin-sections/header.php'); ?>

<div class="main-content">
    <div class="wrapper">
        <h1>Manage Options</h1>

        <br /><br />
        <?php 
        
            if(isset($_SESSION['add']))
            {
                echo $_SESSION['add'];
                unset($_SESSION['add']);
            }

            if(isset($_SESSION['remove']))
            {
                echo $_SESSION['remove'];
                unset($_SESSION['remove']);
            }

            if(isset($_SESSION['delete']))
            {
                echo $_SESSION['delete'];
                unset($_SESSION['delete']);
            }

            if(isset($_SESSION['no-option-found']))
            {
                echo $_SESSION['no-option-found'];
                unset($_SESSION['no-option-found']);
            }

            if(isset($_SESSION['update']))
            {
                echo $_SESSION['update'];
                unset($_SESSION['update']);
            }

            if(isset($_SESSION['upload']))
            {
                echo $_SESSION['upload'];
                unset($_SESSION['upload']);
            }

            if(isset($_SESSION['failed-remove']))
            {
                echo $_SESSION['failed-remove'];
                unset($_SESSION['failed-remove']);
            }
        
        ?>
        <br><br>

                <!-- Button to Add Option -->
                <a href="<?php echo SITEURL; ?>admin/add-option.php" class="btn-primary">Add Option</a>

                <br /><br /><br />

                <table class="tbl-full">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Image</th>
                        <th>Actions</th>
                    </tr>

                    <?php 

                        //Query to Get all Options from Database
                        $sql = "SELECT * FROM `option`";

                        //Execute Query
                        $res = mysqli_query($conn, $sql);

                        //Count Rows
                        $count = mysqli_num_rows($res);

                        //Create Serial Number Variable and assign value as 1
                        $sn=1;

                        //Check whether we have data in database or not
                        if($count>0)
                        {
                            //We have data in database
                            //get the data and display
                            while($row=mysqli_fetch_assoc($res))
                            {
                                $id = $row['id'];
                                $name = $row['optionName'];
                                $description = $row['optionDesc'];
                                $image_name = $row['optionImg'];

                                ?>

                                    <tr>
                                        <td><?php echo $sn++; ?>. </td>
                                        <td><?php echo $name; ?></td>
                                        <td><?php echo $description; ?></td>

                                        <td>

                                            <?php  
                                                //Check whether image name is available or not
                                                if($image_name!="")
                                                {
                                                    //Display the Image
                                                    ?>
                                                    
                                                    <img src="<?php echo SITEURL; ?>images/options/<?php echo $image_name; ?>" width="100px" >
                                                    
                                                    <?php
                                                }
                                                else
                                                {
                                                    //Display the Message
                                                    echo "<div class='error'>Image not Added.</div>";
                                                }
                                            ?>

                                        </td>

                                        <td>
                                            <a href="<?php echo SITEURL; ?>admin/update-option.php?id=<?php echo $id; ?>" class="btn-secondary">Update Option</a>
                                            <a href="<?php echo SITEURL; ?>admin/delete-option.php?id=<?php echo $id; ?>&image_name=<?php echo $image_name; ?>" class="btn-danger">Delete Option</a>
                                        </td>
                                    </tr>

                                <?php

                            }
                        }
                        else
                        {
                            //We do not have data
                            //We'll display the message inside table
                            ?>

                            <tr>
                                <td colspan="5"><div class="error">No Options Added.</div></td>
                            </tr>

                            <?php
                        }
                    
                    ?>

                    

                    
                </table>
    </div>
    
</div>

<?php include('admin-sections/footer.php'); ?>
