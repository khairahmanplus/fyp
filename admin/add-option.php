<?php include('admin-sections/header.php'); ?>

<div class="main-content">
    <div class="wrapper">
        <h1>Add Option</h1>

        <br><br>

        <?php 
        
            if(isset($_SESSION['add']))
            {
                echo $_SESSION['add'];
                unset($_SESSION['add']);
            }

            if(isset($_SESSION['upload']))
            {
                echo $_SESSION['upload'];
                unset($_SESSION['upload']);
            }
        
        ?>

        <br><br>

        <!-- Add Option Form Starts -->
        <form action="" method="POST" enctype="multipart/form-data">

            <table class="tbl-30">
                <tr>
                    <td>Name: </td>
                    <td>
                        <input type="text" name="name" placeholder="Option Name">
                    </td>
                </tr>

                <tr>
                    <td>Description: </td>
                    <td>
                        <input type="text" name="description" placeholder="Option Description">
                    </td>
                </tr>

                <tr>
                    <td>Select Image: </td>
                    <td>
                        <input type="file" name="image">
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <input type="submit" name="submit" value="Add Option" class="btn-secondary">
                    </td>
                </tr>

            </table>

        </form>
        <!-- Add Option Form Ends -->

        <?php 
        
            //Check whether the Submit Button is Clicked or Not
            if(isset($_POST['submit']))
            {
                //1. Get the Value from Option Form
                $name = $_POST['name'];
                $description = $_POST['description'];

                //Check whether the image is selected or not and set the value for image name accordingly
                if(isset($_FILES['image']['name']))
                {
                    //Upload the Image
                    //To upload image we need image name, source path and destination path
                    $image_name = $_FILES['image']['name'];
                    
                    // Upload the Image only if image is selected
                    if($image_name != "")
                    {
                        //Auto Rename our Image
                        $ext = end(explode('.', $image_name));

                        //Rename the Image
                        $image_name = "Option_".rand(000, 999).'.'.$ext; 

                        $source_path = $_FILES['image']['tmp_name'];

                        $destination_path = "../images/options/".$image_name;

                        //Finally Upload the Image
                        $upload = move_uploaded_file($source_path, $destination_path);

                        //Check whether the image is uploaded or not
                        //And if the image is not uploaded then we will stop the process and redirect with error message
                        if($upload==false)
                        {
                            //Set message
                            $_SESSION['upload'] = "<div class='error'>Failed to Upload Image. </div>";
                            //Redirect to Add Option Page
                            header('location:'.SITEURL.'admin/add-option.php');
                            //Stop the Process
                            die();
                        }
                    }
                }
                else
                {
                    //Don't Upload Image and set the image_name value as blank
                    $image_name="";
                }

                //2. Create SQL Query to Insert Option into Database
                $sql = "INSERT INTO `option` SET 
                    optionName='$name',
                    optionDesc='$description',
                    optionImg='$image_name'
                ";

                //3. Execute the Query and Save in Database
                $res = mysqli_query($conn, $sql);

                //4. Check whether the query executed or not and data added or not
                if($res==true)
                {
                    //Query Executed and Option Added
                    $_SESSION['add'] = "<div class='success'>Option Added Successfully.</div>";
                    //Redirect to Manage Option Page
                    header('location:'.SITEURL.'admin/manage-option.php');
                }
                else
                {
                    //Failed to Add Option
                    $_SESSION['add'] = "<div class='error'>Failed to Add Option.</div>";
                    //Redirect to Add Option Page
                    header('location:'.SITEURL.'admin/add-option.php');
                }
            }
        
        ?>

    </div>
</div>

<?php include('admin-sections/footer.php'); ?>
