<?php include('admin-sections/header.php'); ?>

<div class="main-content">
    <div class="wrapper">
        <h1>Manage Food Items</h1>

        <br /><br />

        <!-- Button to Add Food -->
        <a href="add-food.php" class="btn-primary">Add Food</a>

        <br /><br /><br />

        <?php
        if (isset($_SESSION['add'])) {
            echo $_SESSION['add'];
            unset($_SESSION['add']);
        }

        if (isset($_SESSION['delete'])) {
            echo $_SESSION['delete'];
            unset($_SESSION['delete']);
        }

        if (isset($_SESSION['upload'])) {
            echo $_SESSION['upload'];
            unset($_SESSION['upload']);
        }

        if (isset($_SESSION['unauthorize'])) {
            echo $_SESSION['unauthorize'];
            unset($_SESSION['unauthorize']);
        }

        if (isset($_SESSION['update'])) {
            echo $_SESSION['update'];
            unset($_SESSION['update']);
        }
        ?>

        <!-- Filter by Category Form -->
        <form method="GET" action="" id="filterForm">
            <label for="category">Filter by Category:</label>
            <select name="category" id="category">
                <option <?php if (!isset($_GET['category']) || (isset($_GET['category']) && $_GET['category'] == 'all')) {echo "selected";} ?> value="all">All</option>
                <?php
                $categorySql = "SELECT * FROM category";
                $categoryResult = mysqli_query($conn, $categorySql);

                while ($categoryRow = mysqli_fetch_assoc($categoryResult)) {
                    $categoryId = $categoryRow['id'];
                    $categoryName = $categoryRow['title'];
                    echo "<option " . (isset($_GET['category']) && $_GET['category'] == $categoryId ? "selected" : "") . " value='$categoryId'>$categoryName</option>";
                }
                ?>
            </select>
        </form>

        <br /><br />

        <table class="tbl-full">
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Price</th>
                <th>Image</th>
                <th>Available</th>
                <th>Featured</th>
                <th>Reviews</th>
                <th>Actions</th>
            </tr>

            <?php
            $categoryFilter = isset($_GET['category']) && $_GET['category'] !== 'all' ? " WHERE category_id = {$_GET['category']}" : "";
            $sql = "SELECT * FROM food" . $categoryFilter;

            $res = mysqli_query($conn, $sql);

            $count = mysqli_num_rows($res);
            $sn = 1;

            if ($count > 0) {
                while ($row = mysqli_fetch_assoc($res)) {
                    $id = $row['id'];
                    $title = $row['title'];
                    $price = $row['price'];
                    $image_name = $row['image_name'];
                    $active = $row['active'];
                    $featured = $row['featured'];
                    ?>

                    <tr>
                        <td><?php echo $sn++; ?>. </td>
                        <td><?php echo $title; ?></td>
                        <td>RM<?php echo $price; ?></td>
                        <td>
                            <?php
                            if ($image_name == "") {
                                echo "<div class='error'>Image not Added.</div>";
                            } else {
                                ?>
                                <img src="../images/food/<?php echo $image_name; ?>" width="100px">
                            <?php
                            }
                            ?>
                        </td>
                        <td><?php echo $active; ?></td>
                        <td><?php echo $featured; ?></td>
                        <td>
                            <button class="btn-secondary toggle-reviews" data-food-id="<?php echo $id; ?>">Show Reviews</button>
                            <div class="reviews" id="reviews-<?php echo $id; ?>" style="display: none;">
                                <?php
                                $review_sql = "SELECT r.rating, r.reviewMessage, r.timeSubmitted, c.username 
                                               FROM review r 
                                               JOIN customers c ON r.custId = c.id 
                                               WHERE r.foodId = $id 
                                               ORDER BY r.timeSubmitted DESC";
                                $review_res = mysqli_query($conn, $review_sql);

                                if (mysqli_num_rows($review_res) > 0) {
                                    while ($review_row = mysqli_fetch_assoc($review_res)) {
                                        echo "<div class='review'>";
                                        echo "<p><strong>{$review_row['username']}</strong> - {$review_row['timeSubmitted']}</p>";
                                        echo "<p>Rating: {$review_row['rating']}/10</p>";
                                        echo "<p>{$review_row['reviewMessage']}</p>";
                                        echo "</div>";
                                    }
                                } else {
                                    echo "<p>No reviews yet.</p>";
                                }
                                ?>
                            </div>
                        </td>
                        <td>
                            <a href="update-food.php?id=<?php echo $id; ?>" class="btn-secondary">Update Food</a>
                            <a href="delete-food.php?id=<?php echo $id; ?>&image_name=<?php echo $image_name; ?>" class="btn-danger">Delete Food</a>
                        </td>
                    </tr>

            <?php
                }
            } else {
                echo "<tr> <td colspan='8' class='error'> Food not Added Yet. </td> </tr>";
            }
            ?>
        </table>

        <script>
            document.getElementById('category').addEventListener('change', function() {
                document.getElementById('filterForm').submit();
            });

            // Toggle reviews visibility
            document.querySelectorAll('.toggle-reviews').forEach(button => {
                button.addEventListener('click', function() {
                    const foodId = this.getAttribute('data-food-id');
                    const reviewsDiv = document.getElementById('reviews-' + foodId);
                    if (reviewsDiv.style.display === 'none') {
                        reviewsDiv.style.display = 'block';
                        this.textContent = 'Hide Reviews';
                    } else {
                        reviewsDiv.style.display = 'none';
                        this.textContent = 'Show Reviews';
                    }
                });
            });
        </script>
        
        <style>
            .review {
                border-bottom: 1px solid #ddd;
                margin-bottom: 10px;
                padding-bottom: 10px;
                font-size: 14px;
            }
            .reviews {
                font-size: 14px;
                margin-top: 10px;
            }
        </style>
    </div>
</div>

<?php include('admin-sections/footer.php'); ?>
