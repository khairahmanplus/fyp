<?php 
    include('../config/constants.php');
    //1. Destory the Session
    session_destroy(); //Unsets $_SESSION['admin']

    //2. REdirect to Login Page
    header('location:login.php');

?>