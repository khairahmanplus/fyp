<?php include('admin-sections/header.php'); ?>
<div class="main-content">
    <div class="wrapper">
        <h1>Manage Coupons</h1>

        <br /><br />

        <?php
        if (isset($_SESSION['add'])) {
            echo $_SESSION['add'];
            unset($_SESSION['add']);
        }

        if (isset($_SESSION['delete'])) {
            echo $_SESSION['delete'];
            unset($_SESSION['delete']);
        }

        if (isset($_SESSION['update'])) {
            echo $_SESSION['update'];
            unset($_SESSION['update']);
        }
        ?>
<br /><br />
        <!-- Button to Add Coupon -->
        <a href="#" class="btn-primary" onclick="showAddCouponForm()">Add Coupon</a>

        <br /><br />

        <table class="tbl-full">
            <tr>
                <th>Coupon Code</th>
                <th>Discount Value</th>
                <th>Is Active</th>
                <th>Actions</th>
            </tr>

            <?php
            // Fetch all coupons
            $sql = "SELECT * FROM coupon";
            $res = mysqli_query($conn, $sql);
            $count = mysqli_num_rows($res);

            if ($count > 0) {
                while ($row = mysqli_fetch_assoc($res)) {
                    $couponCode = $row['couponCode'];
                    $discountValue = $row['discountValue'];
                    $isActive = $row['isActive'];
                    ?>

                    <tr>
                        <td><?php echo $couponCode; ?></td>
                        <td><?php echo $discountValue; ?></td>
                        <td><?php echo $isActive ? 'Yes' : 'No'; ?></td>
                        <td>
                            <a href="update-coupon.php?couponCode=<?php echo $couponCode; ?>" class="btn-secondary">Update</a>
                            <a href="delete-coupon.php?couponCode=<?php echo $couponCode; ?>" class="btn-danger">Delete</a>
                        </td>
                    </tr>

            <?php
                }
            } else {
                echo "<tr> <td colspan='4' class='error'> No Coupons Added Yet. </td> </tr>";
            }
            ?>
        </table>

        <br /><br />

        <!-- Add Coupon Form -->
        <div id="addCouponForm" style="display: none;">
            <h2>Add Coupon</h2>
            <form action="" method="POST">
                <table class="tbl-30">
                    <tr>
                        <td>Coupon Code: </td>
                        <td><input type="text" name="couponCode" placeholder="Enter Coupon Code"></td>
                    </tr>

                    <tr>
                        <td>Discount Value (%): </td>
                        <td><input type="number" name="discountValue" placeholder="Enter Discount Value" step="0.01" min="0" max="100"></td>
                    </tr>

                    <tr>
                        <td>Is Active: </td>
                        <td>
                            <input type="radio" name="isActive" value="1"> Yes
                            <input type="radio" name="isActive" value="0"> No
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <input type="submit" name="submit" value="Add Coupon" class="btn-primary">
                        </td>
                    </tr>
                </table>
            </form>
        </div>

        <?php
        // Check whether the Submit button is clicked or not
        if (isset($_POST['submit'])) {
            // Get the data from Form
            $couponCode = mysqli_real_escape_string($conn, $_POST['couponCode']);
            $discountValue = mysqli_real_escape_string($conn, $_POST['discountValue']);
            $isActive = isset($_POST['isActive']) ? $_POST['isActive'] : 0;

            // SQL Query to Save the data into database
            $sql2 = "INSERT INTO coupon (couponCode, discountValue, isActive) VALUES ('$couponCode', '$discountValue', '$isActive')";

            // Execute the Query
            $res2 = mysqli_query($conn, $sql2);

            // Check whether the query executed successfully or not
            if ($res2 == TRUE) {
                // Query Executed and Coupon Added
                $_SESSION['add'] = "<div class='success'>Coupon Added Successfully.</div>";
                // Redirect to Manage Coupon Page
                header("location:" . SITEURL . 'admin/manage-coupon.php');
            } else {
                // Failed to Add Coupon
                $_SESSION['add'] = "<div class='error'>Failed to Add Coupon.</div>";
                // Redirect to Manage Coupon Page
                header("location:" . SITEURL . 'admin/manage-coupon.php');
            }
        }
        ?>

        <script>
            function showAddCouponForm() {
                var form = document.getElementById('addCouponForm');
                if (form.style.display === 'none' || form.style.display === '') {
                    form.style.display = 'block';
                } else {
                    form.style.display = 'none';
                }
            }
        </script>
    </div>
</div>

<?php include('admin-sections/footer.php'); ?>
