
<hr>
<footer class="w3-row-padding w3-container">
    <div class="w3-third w3-padding-16">
      <h4>Connect with Us</h4>
      <p><a href="#" target="_blank">Instagram</a></p>
      <p><a href="#" target="_blank">Facebook</a></p>
    </div>
    <div class="w3-third w3-padding-16">
      <h4>About Us</h4>
      <p><a href="home.html#about">About Auntie Jue Home Bakes</a></p>
      <p><a href="#">Privacy Policy</a></p>
      <p><a href="#">Terms & Conditions</a></p>
    </div>
    <div class="w3-third w3-padding-16">
      <h4>Customer Care</h4>
      <p><a href="feedback.php">Feedback</a></p>
      <p><a href="#">FAQs</a></p>
      <p><a href="#">Contact Us</a></p>
    </div>
	
    <div class="w3-container  w3-center">  
      <br><p>2022 © Auntie Jue Home Bakes. All Rights Reserved.</p>
    </div>
  

  </footer>

<!-- End page content -->
</div>


</body>
</html>
