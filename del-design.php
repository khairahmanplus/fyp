<?php
include('partials-front/menu.php');

// Check if the user is logged in
if (!isset($_SESSION['username'])) {
    header('location:login.php');
    exit;
}

$cust_username = $_SESSION['username'];

// Check if custom_id is set and numeric
if (isset($_GET['custom_id']) && is_numeric($_GET['custom_id'])) {
    $custom_id = $_GET['custom_id'];
    
    // Fetch customization details to ensure the design belongs to the logged-in user
    $custom_sql = "SELECT * FROM customization WHERE id='$custom_id' AND custId=(SELECT id FROM customers WHERE username='$cust_username')";
    $custom_res = mysqli_query($conn, $custom_sql);
    
    if (mysqli_num_rows($custom_res) > 0) {
        // Perform deletion
        $delete_sql = "DELETE FROM customization WHERE id='$custom_id'";
        $delete_res = mysqli_query($conn, $delete_sql);
        
        if ($delete_res) {
            $_SESSION['order'] = "<div class='success'>Design deleted successfully.</div>";
            header('location:' . SITEURL.'profile.php#design-library');
            exit;
        } else {
            $_SESSION['order'] = "<div class='error'>Failed to delete design. Please try again.</div>";
            header('location:' . SITEURL.'profile.php#design-library');
            exit;
        }
    } else {
        $_SESSION['order'] = "<div class='error'>Unauthorized access to delete this design.</div>";
        header('location:' . SITEURL.'profile.php#design-library');
        exit;
    }
} else {
    $_SESSION['order'] = "<div class='error'>Invalid request to delete design.</div>";
    header('location:' . SITEURL.'profile.php#design-library');
    exit;
}

include('partials-front/footer.php');
?>
