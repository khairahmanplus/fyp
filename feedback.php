<?php include('partials-front/menu.php');
// Process form submission
if(isset($_POST['submit'])) {
    // Get form data
    $custId = mysqli_real_escape_string($conn, $_POST['custId']);
    $feedbackMessage = mysqli_real_escape_string($conn, $_POST['feedbackMessage']);
    $timeSubmitted = date('Y-m-d H:i:s'); // Current date and time

    // Insert feedback into database
    $sql = "INSERT INTO feedback (custId, feedbackMessage, timeSubmitted) VALUES ('$custId', '$feedbackMessage', '$timeSubmitted')";
    $result = mysqli_query($conn, $sql);

    if($result) {
        // Feedback submitted successfully
        $msg = "Thank you for your feedback!";
    } else {
        // Failed to submit feedback
        $error = "Failed to submit feedback. Please try again.";
    }
}
?>

<section class="w3-center w3-white">
        <div class="container">
        <h2>Customer Feedback</h2>
        <?php if (isset($_SESSION['username'])){
            
      
                    //Get the DEtails of the SElected Food
                    $sql = "SELECT * FROM customers WHERE username = '" . $_SESSION['username'] . "'";
                    //Execute the Query
                    $res = mysqli_query($conn, $sql);
                    //Count the rows
                    $count = mysqli_num_rows($res);
                    //CHeck whether the data is available or not
                    if($count==1)
                    {
                        //WE Have DAta
                        //GEt the Data from Database
                        $row = mysqli_fetch_assoc($res);

                        $custId = $row['id'];
                    }?>

    <?php if(isset($msg)): ?>
        <p><?php echo $msg; ?></p>
    <?php endif; ?>
    <?php if(isset($error)): ?>
        <p><?php echo $error; ?></p>
    <?php endif; ?>
    <form method="post" action="">
        <input type="hidden" id="custId" name="custId" value="<?php echo $custId; ?>" required><br><br>

        <textarea id="feedbackMessage" name="feedbackMessage" placeholder="Feel free to give us feedback here!" style="width: 600px; height: 215px" required></textarea><br><br>

        <input type="submit" name="submit" value="Submit Feedback"class="btn btn-primary">
    </form><?php }
    else{
        echo "<p>You are not logged in. <a class='w3-text-green' href='login.php'>Login Here</a></p>";
    }?>
        </div>
</section>

        <div class="container">

        </div>


    <?php include('partials-front/footer.php'); ?>